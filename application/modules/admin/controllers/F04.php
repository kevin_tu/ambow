<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F04 extends MY_Controller_Admin {

	function __construct(){
		parent::__construct();
		$this->load->model('admin/tags_model');
		$this->func = 'F04';
		$this->data['func'] = $this->func;
		$this->check_auth($this->func);
	}

	public function index()
	{
		$this->data['parent_id'] = isset($this->get['parent_id']) ? $this->get['parent_id'] : false;
		$this->data['length'] = isset($this->get['length']) ? $this->get['length'] : 20;
		$this->data['start'] = isset($this->get['start']) ? $this->get['start'] : 0;

		$this->data['parent_tag'] = $this->tags_model->get_row(array('id'=>$this->data['parent_id'],'tag_type'=>$this->func));
		$level_1 = $this->tags_model->get_parents('F99');
		$tags = array();
		foreach ($level_1 as $key => $value) {
			$tags[$value['parent_id']][] = $value;
		}
		// echo json_encode($tags);exit;

		$this->data['level_1'] = $tags;
		// echo json_encode($this->data);exit;
		
		$this->data['content_view'] = $this->load->view('admin/'.$this->func.'/index', $this->data, true);
		$this->load->view('admin/master_view', $this->data, FALSE);
	}

	public function list_json($parent_id=false){
		$tag_type = $this->func;
		$length = isset($this->post['length']) ? $this->post['length'] : 20;
		$start = isset($this->post['start']) ? $this->post['start'] : 0;

		$search = array();
		if(!empty($_POST['search']['value'])){
			for($i=0 ; $i<count($_POST['columns']);$i++){
				if ($_POST['columns'][$i]['searchable']=='true'){
					$column = $_POST['columns'][$i]['data'];
					$search[$column]=$_POST['search']['value'];
				}
			}
		}

		$rs = $this->tags_model->get_rs($tag_type,$search,$length,$start,$parent_id);
		echo json_encode($rs);
		//print_r($rs);
		//$this->data['rs'] = $rs;
	}

	public function edit($id=0){

		if (count($this->post)){
			$additional = array(
				'img'=>@$this->post['img'],
				'description'=>@$this->post['description']
				);
			$tag_data = array(
				'id' => $id,
				'tag_type' => $this->func,
				'status' => @$this->post['status'],
				'title' => @$this->post['title'],
				'parent_id' => @$this->post['parent_id'],
				'additional' => json_encode($additional),
				);

			$tag_id = $this->tags_model->save($tag_data);

			if ($id==0){
				echo "location.replace('" . site_url() . "admin/{$this->func}');";
			}
			exit;
		}
		$search = array('id'=>$id);
		$row = $this->tags_model->get_row(array('id'=>$id,'tag_type'=>$this->func));
		$row['additional'] = json_decode($row['additional'],1);
		$this->data['row'] = $row;
		$level_1 = $this->tags_model->get_parents('F99');
		$tags = array();
		foreach ($level_1 as $key => $value) {
			$tags[$value['parent_id']][] = $value;
		}
		// echo json_encode($tags);exit;
		$this->data['level_1'] = $tags;
		$this->data['tags_all'] = $this->tags_model->get_all('F03', array($id));
		$this->data['content_view'] = $this->load->view('admin/'.$this->func.'/edit', $this->data, true);
		$this->load->view('admin/master_view', $this->data, FALSE);

	}


	public function delete($id=0){
		$tag_data = array('id'=>$id);
		$this->tags_model->delete($tag_data);
	}


	public function sort(){
		$tag_id = $this->post['tag_id'];
		$start = $this->post['start'];
		$this->tags_model->sort($this->func,$tag_id,$start);
	}

	public function publish(){
		$tag_id = $this->post['id'];
		$status = $this->post['status'];
		$this->tags_model->publish($tag_id,$status);
	}
}

/* End of file F04.php */
/* Location: ./application/modules/admin/controllers/F04.php */