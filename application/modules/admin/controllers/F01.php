<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F01 extends MY_Controller_Admin {

	function __construct(){
		parent::__construct();
		$this->load->model('admin/posts_model');
		$this->check_auth('F01');
	}

	public function index()
	{

		if (count($this->post)){
			$this->db->query("delete from meta where meta_key like 'meta_%'");

			$meta = array(
					array('meta_key'=>'meta_site_name','meta_value'=>$this->post['meta_site_name']),
					array('meta_key'=>'meta_keyword','meta_value'=>$this->post['meta_keyword']),
					array('meta_key'=>'meta_description','meta_value'=>$this->post['meta_description']),
					array('meta_key'=>'meta_ogimage','meta_value'=>$this->post['meta_ogimage'])
				);
			$this->db->insert_batch('meta',$meta);
			exit;
		}

		$sql = "select 
			(select meta_value from meta where meta_key='meta_site_name') as meta_site_name,
			(select meta_value from meta where meta_key='meta_keyword') as meta_keyword,
			(select meta_value from meta where meta_key='meta_description') as meta_description,
			(select meta_value from meta where meta_key='meta_ogimage') as meta_ogimage";

		$query = $this->db->query($sql);
		$this->data['row'] = $query->row_array();

		$this->data['content_view'] = $this->load->view('admin/F01/edit', $this->data, true);
		$this->load->view('admin/master_view', $this->data, FALSE);
	}

}

/* End of file F01.php */
/* Location: ./application/controllers/admin/F01.php */