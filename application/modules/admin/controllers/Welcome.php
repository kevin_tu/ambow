<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller_Admin {

	public function index()
	{
		//$this->data['content_view'] = $this->load->view('admin/member_view', $this->data, true);
		$this->load->view('admin/master_view', $this->data, FALSE);
	}

}

/* End of file Welcome.php */
/* Location: ./application/controllers/admin/Welcome.php */