<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F05 extends MY_Controller_Admin {

	function __construct(){
		parent::__construct();
		$this->load->model('admin/posts_model');
		$this->load->model('admin/tags_model');
		$this->func = 'F05';
		$this->data['func'] = $this->func;
		$this->check_auth($this->func);
	}

	public function index()
	{
		$this->data['tag_id'] = isset($this->get['tag_id']) ? $this->get['tag_id'] : 0;
		$this->data['length'] = isset($this->get['length']) ? $this->get['length'] : 20;
		$this->data['start'] = isset($this->get['start']) ? $this->get['start'] : 0;
		$level_1 = $this->tags_model->get_parents('F99');
		$tags = array();
		foreach ($level_1 as $key => $value) {
			$tags[$value['parent_id']][] = $value;
		}
		// echo json_encode($tags);exit;

		$this->data['level_1'] = $tags;
		$this->data['content_view'] = $this->load->view('admin/'.$this->func.'/index', $this->data, true);
		$this->load->view('admin/master_view', $this->data, FALSE);
	}

	public function list_json($tag_id=0){
		$length = isset($this->post['length']) ? $this->post['length'] : 10;
		$start = isset($this->post['start']) ? $this->post['start'] : 0;

		$search = array();
		if(!empty($_POST['search']['value'])){
			for($i=0 ; $i<count($_POST['columns']);$i++){
				if ($_POST['columns'][$i]['searchable']=='true'){
					$column = $_POST['columns'][$i]['data'];
					$search[$column]=$_POST['search']['value'];
				}
			}
		}

		$rs = $this->posts_model->get_rs($this->func,$tag_id,$search,$length,$start,$order_by='post_is_top desc, createtime desc, id desc');
		// $rs = $this->get_rs($this->func,$tag_id,$search,$length,$start,$order_by='sort desc,id desc');
		echo json_encode($rs);
		//print_r($rs);
		//$this->data['rs'] = $rs;
	}

	public function edit($id=0){

		if (count($this->post)){
			$post_data = array(
				'id' => $id,
				'post_type' => $this->func,
				'status' => @$this->post['status'],
				'createtime' => @$this->post['post_date'],
				'title' => @$this->post['post_title']
				);
			$meta_data = array(
				array('meta_key'=>'post_title','meta_value'=>@$this->post['post_title']),
				array('meta_key'=>'post_cover','meta_value'=>@$this->post['post_cover']),
				array('meta_key'=>'post_content','meta_value'=>@$_POST['post_content']),
				array('meta_key'=>'post_template','meta_value'=>json_encode(@$_POST['post_template'])),
				array('meta_key'=>'post_is_top','meta_value'=>@$_POST['post_is_top']),
				array('meta_key'=>'post_url_wechat','meta_value'=>@$_POST['post_url_wechat']),
				array('meta_key'=>'post_url_baijiahao','meta_value'=>@$_POST['post_url_baijiahao']),
				array('meta_key'=>'post_url_sina','meta_value'=>@$_POST['post_url_sina']),
				array('meta_key'=>'post_url_weibo','meta_value'=>@$_POST['post_url_weibo']),
				array('meta_key'=>'post_url_qq','meta_value'=>@$_POST['post_url_qq']),
				array('meta_key'=>'post_url_toutiao','meta_value'=>@$_POST['post_url_toutiao']),
				array('meta_key'=>'post_url_sohu','meta_value'=>@$_POST['post_url_sohu']),
				);

			$tag_data = isset($this->post['tags']) ? $this->post['tags'] : array();
			$tag_data[] = 0;
			


			$post_id = $this->posts_model->save($post_data,$meta_data,$tag_data);
			$this->db->query("delete from {$this->func} where id={$post_id}");
			$this->db->query("replace into {$this->func} select * from {$this->func}_view where id={$post_id}");

			
			//取得語系
			$sql = "select * from tags x1 inner join tag_assoc x2 on x2.tag_id=x1.id where x2.post_id='{$post_id}' and x1.id=21";
			$query = $this->db->query($sql);
			if ($query->num_rows()>0) {
				//中文
				$lang_id = 21;
			}
			else{
				$lang_id = 22;
			}
			//清除其他置頂
			if (@$this->post['post_is_top']==1) {
				
				//目前置頂
				$sql = "SELECT x1.* FROM meta x1
				INNER JOIN tag_assoc x2 ON x2.`post_id`=x1.`parent_id`
				WHERE x1.`meta_key`='post_is_top'
				AND x1.`meta_value`=1
				AND x1.`parent_id`<>'{$post_id}'
				AND x2.`tag_id`={$lang_id}";
			
				$query = $this->db->query($sql);
				if ($query->num_rows()>0) {
					$row = $query->row_array();
					$this->db->where('parent_id', $row['parent_id']);
					$this->db->where('meta_key', 'post_is_top');
					$this->db->update('meta', array('meta_value'=>0));

					$this->db->query("delete from {$this->func} where id={$row['parent_id']}");
					$this->db->query("replace into {$this->func} select * from {$this->func}_view where id={$row['parent_id']}");
				}
			}

			if ($id==0){
				// echo "location.replace('" . site_url() . "admin/{$this->func}/edit/{$post_id}');";
				echo "location.replace('" . site_url() . "admin/{$this->func}');";
			}
			exit;
		}
		$search = array('id'=>$id);
		$row = $this->posts_model->get_row($this->func,array('id'=>$id));
		if (!strlen($row['createtime'])){
			$row['createtime'] = $row['post_startdate'] = $row['post_enddate'] = date('Y-m-d');
		}
		$row['tags_selected'] = $this->tags_model->get_rs_selected($id);

		//樣板模組
		$modal_data = array();
		$post_template = json_decode(@$row['post_template'], 1);
		$template_data = is_array($post_template) ? $post_template : array();


		$template_html = array();
		foreach ($template_data as $data) {
			$html = '<div class="panel panel-default">' . 
				$this->load->view('admin/template/view_' . $data['template_id'], $data, true) . 
				'</div>';
			$template_html[] = $html;
		}
		//echo json_encode($row);exit;
		$this->data['row'] = $row;
		$level_1 = $this->tags_model->get_parents('F99');
		$tags = array();
		foreach ($level_1 as $key => $value) {
			$tags[$value['parent_id']][] = $value;
		}
		// echo json_encode($tags);exit;
		$this->data['level_1'] = $tags;
		$this->data['category'] = $this->tags_model->get_parents('F04');
		$this->data['template_html'] = implode('', $template_html);
		$this->data['content_view'] = $this->load->view('admin/'.$this->func.'/edit', $this->data, true);
		$this->load->view('admin/master_view', $this->data, FALSE);
		//echo in_array("1",$this->data['tags_selected']);

	}


	public function delete($id=0){
		$post_data = array('id'=>$id);
		$this->posts_model->delete($post_data);
		$this->db->query("delete from {$this->func} where id={$id}");
	}


	public function sort($tag_id){
		$post_id = $this->post['post_id'];
		$start = $this->post['start'];
		$this->posts_model->sort($this->func,$post_id,$tag_id,$start);
		$this->db->query("delete from {$this->func} where id in(".implode(',',$post_id).")");
		$this->db->query("replace into {$this->func} select * from {$this->func}_view where id in(".implode(',',$post_id).")");
	}

	public function publish(){
		$post_id = $this->post['id'];
		$status = $this->post['status'];
		$this->posts_model->publish($post_id,$status);
		$this->db->query("delete from {$this->func} where id={$post_id}");
		$this->db->query("replace into {$this->func} select * from {$this->func}_view where id={$post_id}");
	}

	public function set_top(){
		$post_id = $this->post['id'];
		$post_is_top = $this->post['is_top'];
		
		
		//補上post_is_top
		$query = $this->db
			->where('parent_id', $post_id)
			->where('meta_key', 'post_is_top')
			->get('meta');
		if ($query->num_rows()==0) {
			$data = array(
				'meta_key'=> 'post_is_top', 
				'meta_value'=> 0,
				'parent_id'=> $post_id
			);
			$this->db->insert('meta', $data);
		}

		//取得語系
		$sql = "select * from tags x1 inner join tag_assoc x2 on x2.tag_id=x1.id where x2.post_id='{$post_id}' and x1.id=21";
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			//中文
			$lang_id = 21;
		}
		else{
			$lang_id = 22;
		}

		//目前置頂
		$sql = "SELECT x1.* FROM meta x1
			INNER JOIN tag_assoc x2 ON x2.`post_id`=x1.`parent_id`
			WHERE x1.`meta_key`='post_is_top'
			AND x1.`meta_value`=1
			AND x2.`tag_id`={$lang_id}";
		
		$query = $this->db->query($sql);
		if ($query->num_rows()>0) {
			$row = $query->row_array();
			$this->db->where('parent_id', $row['parent_id']);
			$this->db->where('meta_key', 'post_is_top');
			$this->db->update('meta', array('meta_value'=>0));
			
			$this->db->query("delete from {$this->func} where id={$row['parent_id']}");
			$this->db->query("replace into {$this->func} select * from {$this->func}_view where id={$row['parent_id']}");
		}



		
		$this->db
			->where('parent_id', $post_id)
			->where('meta_key', 'post_is_top')
			->update('meta', array('meta_value'=> $post_is_top));
			echo $this->db->last_query();
		$this->db->query("delete from {$this->func} where id={$post_id}");
		$this->db->query("replace into {$this->func} select * from {$this->func}_view where id={$post_id}");
	}

	public function get_children_level(){
		$parent_id = $this->post['level_1'];
		$result = $this->tags_model->get_rs_by_parent_id($parent_id);
		echo json_encode($result);
	}
	public function preview($id){
		
		$this->data['body_class'] = 'news';
		$this->data['meta']['page_title'] .= '-安博资讯';

		$this->db
			->select("F05.*")
			->join('tags', "tags.id=F05.tag_id")
			->where('tags.parent_id', 21) //語系
			->where('F05.id', $id);
			// ->where('F05.status', 1);
		$query = $this->db->get('F05');
		if ($query->num_rows()==0) {
			show_404();
		}

		$news = $query->row_array();

		$template_data = json_decode($news['post_template'], 1);

		$template_html = array();
		if (!empty($template_data)) {
			foreach ($template_data as $value) {
				$template_id = $value['template_id'];
				if (isset($value['list_data'])) {
					$new_list_data = array();
					foreach ($value['list_data'] as $value2) {
						$new_list_data[] = $value2;
						$modal_data['list_data'][$value2['id']] = $value2;
					}
					$value['list_data'] = $new_list_data;
				}
				$template_html[] = $this->load->view('/front/template/view_' . $template_id, $value, true);
			}
		}
		$this->data['news'] = $news;
		$this->data['template_html'] = $template_html;
		$this->data['content_view'] = $this->load->view('/news_detail_view', $this->data, true);
		$this->load->view('/front/master_view', $this->data, FALSE);
	}

	public function duplicate($prod_id){
		$post_data = $this->db->where('id', $prod_id)->get('posts')->row_array();
		$post_data['id'] = 0;
		$post_data['status'] = 0;
		$post_data['title'] .= ' - 複製';
		$post_data['createtime'] = date('Y-m-d H:i:s');
		// echo json_encode($post_data); exit;

		$meta_data = $this->db->select('meta_key, meta_value')->where('parent_id', $prod_id)->get('meta')->result_array();
		foreach ($meta_data as $key => $value) {
			if ($value['meta_key']=='post_title') {
				$value['meta_value'] .= ' - 複製';
				$meta_data[$key] = $value;
			}
		}

		$tag_data = isset($this->post['tags']) ? $this->post['tags'] : array();
		$tag_data[] = 0;

		$tags = $this->db->where('post_id', $prod_id)->get('tag_assoc')->result_array();
		foreach ($tags as $key => $value) {
			$tag_data[] = $value['tag_id'];
		}

		// echo json_encode($tag_data); exit;

		$post_id = $this->posts_model->save($post_data,$meta_data,$tag_data);
		$this->db->query("delete from {$this->func} where id={$post_id}");
		$this->db->query("replace into {$this->func} select * from {$this->func}_view where id={$post_id}");
	}

}

/* End of file F05.php */
/* Location: ./application/modules/admin/controllers/F05.php */