<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F06 extends MY_Controller_Admin {

	function __construct(){
		parent::__construct();
		$this->load->model('admin/posts_model');
		$this->load->model('admin/tags_model');
		$this->func = 'F06';
		$this->data['func'] = $this->func;
		$this->check_auth($this->func);
	}

	public function index()
	{
		$this->data['tag_id'] = isset($this->get['tag_id']) ? $this->get['tag_id'] : 0;
		$this->data['length'] = isset($this->get['length']) ? $this->get['length'] : 20;
		$this->data['start'] = isset($this->get['start']) ? $this->get['start'] : 0;
		$level_1 = $this->tags_model->get_parents('F99');
		$tags = array();
		foreach ($level_1 as $key => $value) {
			$tags[$value['parent_id']][] = $value;
		}
		// echo json_encode($tags);exit;

		$this->data['level_1'] = $tags;
		$this->data['content_view'] = $this->load->view('admin/'.$this->func.'/index', $this->data, true);
		$this->load->view('admin/master_view', $this->data, FALSE);
	}

	public function list_json($tag_id=0){
		$length = isset($this->post['length']) ? $this->post['length'] : 10;
		$start = isset($this->post['start']) ? $this->post['start'] : 0;

		$search = array();
		if(!empty($_POST['search']['value'])){
			for($i=0 ; $i<count($_POST['columns']);$i++){
				if ($_POST['columns'][$i]['searchable']=='true'){
					$column = $_POST['columns'][$i]['data'];
					$search[$column]=$_POST['search']['value'];
				}
			}
		}

		$rs = $this->posts_model->get_rs($this->func,$tag_id,$search,$length,$start,$order_by='sort desc,id desc');
		// $rs = $this->get_rs($this->func,$tag_id,$search,$length,$start,$order_by='sort desc,id desc');
		echo json_encode($rs);
		//print_r($rs);
		//$this->data['rs'] = $rs;
	}

	public function edit($id=0){

		if (count($this->post)){
			$post_data = array(
				'id' => $id,
				'post_type' => $this->func,
				'status' => @$this->post['status'],
				'createtime' => @$this->post['post_date'],
				'title' => @$this->post['post_title']
				);
			$meta_data = array(
				array('meta_key'=>'post_title','meta_value'=>@$this->post['post_title']),
				array('meta_key'=>'post_cover','meta_value'=>@$this->post['post_cover']),
				array('meta_key'=>'post_content','meta_value'=>@$_POST['post_content']),
				array('meta_key'=>'post_link','meta_value'=>@$_POST['post_link']),
				array('meta_key'=>'post_link_target','meta_value'=>@$_POST['post_link_target']),
				);

			$tag_data = isset($this->post['tags']) ? $this->post['tags'] : array();
			$tag_data[] = 0;

			$post_id = $this->posts_model->save($post_data,$meta_data,$tag_data);
			$this->db->query("delete from {$this->func} where id={$post_id}");
			$this->db->query("replace into {$this->func} select * from {$this->func}_view where id={$post_id}");

			if ($id==0){
				// echo "location.replace('" . site_url() . "admin/{$this->func}/edit/{$post_id}');";
				echo "location.replace('" . site_url() . "admin/{$this->func}');";
			}
			exit;
		}
		$search = array('id'=>$id);
		$row = $this->posts_model->get_row($this->func,array('id'=>$id));
		if (!strlen($row['createtime'])){
			$row['createtime'] = $row['post_startdate'] = $row['post_enddate'] = date('Y-m-d');
		}
		$row['tags_selected'] = $this->tags_model->get_rs_selected($id);

		//echo json_encode($row);exit;
		$this->data['row'] = $row;
		$level_1 = $this->tags_model->get_parents('F99');
		$tags = array();
		foreach ($level_1 as $key => $value) {
			$tags[$value['parent_id']][] = $value;
		}
		// echo json_encode($tags);exit;
		$this->data['level_1'] = $tags;
		$this->data['colors'] = $this->tags_model->get_parents('F12');
		$this->data['supply'] = $this->db->get('manager')->result_array();
		$this->data['content_view'] = $this->load->view('admin/'.$this->func.'/edit', $this->data, true);
		$this->load->view('admin/master_view', $this->data, FALSE);
		//echo in_array("1",$this->data['tags_selected']);

	}


	public function delete($id=0){
		$post_data = array('id'=>$id);
		$this->posts_model->delete($post_data);
		$this->db->query("delete from {$this->func} where id={$id}");
	}


	public function sort($tag_id){
		$post_id = $this->post['post_id'];
		$start = $this->post['start'];
		$this->posts_model->sort($this->func,$post_id,$tag_id,$start);
		$this->db->query("delete from {$this->func} where id in(".implode(',',$post_id).")");
		$this->db->query("replace into {$this->func} select * from {$this->func}_view where id in(".implode(',',$post_id).")");
	}

	public function publish(){
		$post_id = $this->post['id'];
		$status = $this->post['status'];
		$this->posts_model->publish($post_id,$status);
		$this->db->query("delete from {$this->func} where id={$post_id}");
		$this->db->query("replace into {$this->func} select * from {$this->func}_view where id={$post_id}");
	}

	public function get_children_level(){
		$parent_id = $this->post['level_1'];
		$result = $this->tags_model->get_rs_by_parent_id($parent_id);
		echo json_encode($result);
	}
	public function preview($prod_id){
		$post = $this->db
			->where('tag_id', 0)
			// ->where('status', 1)
			->where('id', $prod_id)
			->get('F13')->row_array();

		if (empty($post)) {
			show_404();
		}
		else{
			$this->data['body_class'] = 'news';
			$this->data['post'] = $post;
			$this->data['content_view'] = $this->load->view('news/detail_view', $this->data, true);
			$this->load->view('master_view', $this->data, FALSE);
		}
	}

	public function duplicate($prod_id){
		$post_data = $this->db->where('id', $prod_id)->get('posts')->row_array();
		$post_data['id'] = 0;
		$post_data['status'] = 0;
		$post_data['title'] .= ' - 複製';
		$post_data['createtime'] = date('Y-m-d H:i:s');
		// echo json_encode($post_data); exit;

		$meta_data = $this->db->select('meta_key, meta_value')->where('parent_id', $prod_id)->get('meta')->result_array();
		foreach ($meta_data as $key => $value) {
			if ($value['meta_key']=='post_title') {
				$value['meta_value'] .= ' - 複製';
				$meta_data[$key] = $value;
			}
		}

		$tag_data = isset($this->post['tags']) ? $this->post['tags'] : array();
		$tag_data[] = 0;

		$tags = $this->db->where('post_id', $prod_id)->get('tag_assoc')->result_array();
		foreach ($tags as $key => $value) {
			$tag_data[] = $value['tag_id'];
		}

		// echo json_encode($tag_data); exit;

		$post_id = $this->posts_model->save($post_data,$meta_data,$tag_data);
		$this->db->query("delete from {$this->func} where id={$post_id}");
		$this->db->query("replace into {$this->func} select * from {$this->func}_view where id={$post_id}");
	}

}

/* End of file F06.php */
/* Location: ./application/modules/admin/controllers/F06.php */