<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends MY_Controller {

	public function index()
	{
		unset($_SESSION['admin_user']);
		header('location:login');
	}

}

/* End of file Logout.php */
/* Location: ./application/controllers/admin/Logout.php */