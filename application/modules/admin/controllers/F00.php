<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F00 extends MY_Controller_Admin {

	function __construct(){
		parent::__construct();
		$this->load->model('admin/manager_model');
		$this->func = 'F00';
		$this->data['func'] = $this->func;
		$this->check_auth($this->func);
	}

	public function index()
	{
		$this->data['tag_id'] = isset($this->get['tag_id']) ? $this->get['tag_id'] : 0;
		$this->data['length'] = isset($this->get['length']) ? $this->get['length'] : 10;
		$this->data['start'] = isset($this->get['start']) ? $this->get['start'] : 0;
		$this->data['content_view'] = $this->load->view('admin/'.$this->func.'/index', $this->data, true);
		$this->load->view('admin/master_view', $this->data, FALSE);
	}

	public function list_json($tag_id=0){
		$length = isset($this->post['length']) ? $this->post['length'] : 10;
		$start = isset($this->post['start']) ? $this->post['start'] : 0;

		$search = array();
		if(!empty($_POST['search']['value'])){
			for($i=0 ; $i<count($_POST['columns']);$i++){
				if ($_POST['columns'][$i]['searchable']=='true'){
					$column = $_POST['columns'][$i]['data'];
					$search[$column]=$_POST['search']['value'];
				}
			}
		}

		$rs = $this->manager_model->get_rs($this->func,$tag_id,$search,$length,$start);
		echo json_encode($rs);
		//print_r($rs);
		//$this->data['rs'] = $rs;
	}

	public function edit($id=0){

		if (count($this->post)){
			$post_data = array(
				'id' => $id,
				'userid' => @$this->post['userid'],
				'status' => @$this->post['status'],
				'auth' => isset($this->post['auth']) ? json_encode(@$this->post['auth']) : '[]'
				);

			if ($this->post['userpwd']!=''){
				$post_data['userpwd'] = sha1(md5(@$this->post['userpwd']));
			}

			$row = $this->db->where('userid',$post_data['userid'])->get('manager')->result_array();
			if (count($row) && $id==0){
				echo 'alert("帳號重複")';
				exit;
			}
			$post_id = $this->manager_model->save($post_data,array(),array());

            echo "alert('儲存完成',  '',  'success');";
			if ($id==0){
				echo "location.replace('" . site_url() . "admin/{$this->func}/edit/{$post_id}');";
			}
			exit;
		}
		$search = array('id'=>$id);
		$row = $this->manager_model->get_row($this->func,$search);
		$row['auth'] = strlen($row['auth']) ? json_decode($row['auth'],1) : array();
		$this->data['row'] = $row;
		$this->data['content_view'] = $this->load->view('admin/'.$this->func.'/edit', $this->data, true);
		$this->load->view('admin/master_view', $this->data, FALSE);
		//echo in_array("1",$this->data['tags_selected']);

	}


	public function delete($id=0){
		$post_data = array('id'=>$id);
		$this->manager_model->delete($post_data);
	}


	public function sort($tag_id){
		$post_id = $this->post['post_id'];
		$start = $this->post['start'];
		$this->posts_model->sort($this->func,$post_id,$tag_id,$start);
	}

	public function publish(){
		$post_id = $this->post['id'];
		$status = $this->post['status'];
		$this->posts_model->publish($post_id,$status);
	}

}

/* End of file F13.php */
/* Location: ./application/controllers/admin/F13.php */