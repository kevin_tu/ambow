<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends CI_Controller {

	public function load($template_id)
	{
		$settings = array(
			'options'=>array(),
			'txt'=>array(),
			'img'=>array(),
			'video'=>array(),
			'list_data'=>array()
			);
		$this->load->view('admin/template/view_' . $template_id, $settings, FALSE);
	}

}

/* End of file Template.php */
/* Location: ./application/controllers/admin/Template.php */