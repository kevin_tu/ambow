<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends MY_Controller {
	public $data;
	function __construct(){
        parent::__construct();
        $this->post = $this->input->post(null,true);
		$this->load->model('admin/upload_model','upload_model');
    }

    function image_upload(){
        $this->upload_model->data_key = array('image_width');
        $img = $this->upload_model->img_upload('myImg');

        //縮圖
        $thumbnail_size = json_decode($this->post['thumbnail_size']);
        if (count($thumbnail_size)){
            $this->upload_model->size = $thumbnail_size;
            $this->upload_model->img_fit($img['file']);
            $file_name = explode('/',$img['file']);

            $img = array('img_url'=>site_url().'upload/','file'=>$file_name[0].'/'.file_core_name($file_name[1]).'_'.$thumbnail_size[0].'.'. file_extension($file_name[1]));
            //$img = array('img_url'=>site_url().'upload/','file'=>$file_name[0].'/'.file_core_name($file_name[1]).'.'. file_extension($file_name[1]));
        }
        else{
            $file_name = explode('/',$img['file']);
            $img = array('img_url'=>site_url().'upload/','file'=>$file_name[0].'/'.file_core_name($file_name[1]).'.'. file_extension($file_name[1]));
        }
        echo json_encode($img);
        exit;

    }

    function crop_upload(){
        $this->upload_model->data_key = array('image_width');
        $img = $this->upload_model->img_upload('myImg');

        //縮圖
        $thumbnail_size = json_decode($this->post['thumbnail_size']);
        if (count($thumbnail_size)){
            $this->upload_model->size = $thumbnail_size;
            //$this->upload_model->img_fit($img['file']);
            $file_name = explode('/',$img['file']);

            //$img = array('img_url'=>site_url().'upload/','file'=>$file_name[0].'/'.file_core_name($file_name[1]).'_'.$thumbnail_size[0].'.'. file_extension($file_name[1]));
            $img = array('img_url'=>site_url().'upload/','file'=>$file_name[0].'/'.file_core_name($file_name[1]).'.'. file_extension($file_name[1]));
        }
        else{
            $file_name = explode('/',$img['file']);
            $img = array('img_url'=>site_url().'upload/','file'=>$file_name[0].'/'.file_core_name($file_name[1]).'.'. file_extension($file_name[1]));
        }
        echo json_encode($img);
        exit;

    }

    public function crop(){
        $this->load->model('admin/upload_model');
        $this->upload_model->crop();
        exit;
    }

    public function gallery_upload(){
        $this->load->model('admin/upload_model');
        $this->upload_model->data_key = array('image_width');
        $img = $this->upload_model->img_upload('myGalleryUpload_instance');

        //縮圖
        $thumbnail_size = json_decode($this->post['thumbnail_size']);
        if (count($thumbnail_size)){
            $this->upload_model->size = $thumbnail_size;
            $this->upload_model->img_fit($img['file']);
            $file_name = explode('/',$img['file']);

            $img = array('img_url'=>site_url().'upload/','file'=>$file_name[0].'/'.file_core_name($file_name[1]).'_'.$thumbnail_size[0].'.'. file_extension($file_name[1]));
        }
        else{
            $file_name = explode('/',$img['file']);
            $img = array('img_url'=>site_url().'upload/','file'=>$file_name[0].'/'.file_core_name($file_name[1]).'.'. file_extension($file_name[1]));

            $this->load->library('image_lib');
            $this->load->helper('my_file_helper');
            $config['image_library'] = 'gd2';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['source_image'] =  FCPATH . 'upload/' .$file_name[0].'/'.file_core_name($file_name[1]).'.'. file_extension($file_name[1]);
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 150;
            $config['height'] = 150;
            $config['new_image']   = FCPATH . 'upload/thumbnail/' . file_core_name($file_name[1]).'.'. file_extension($file_name[1]);
            $this->image_lib->initialize($config);
            $this->image_lib->fit();
            $this->image_lib->clear();
        }
        echo json_encode($img);
        exit;
    }

    function ck_upload(){
        $this->upload_model->ckupload();
    }

    function file_upload(){
        $file = $this->upload_model->file_upload('myFileUpload_instance');
        //print_r($file);exit;
        $file_name = explode('/',$file['filename']);
        $file = array('img_url'=>site_url().'upload/','file'=>$file_name[0].'/'.file_core_name($file_name[1]).'.'. file_extension($file_name[1]),'org_file_name'=>$file['org_file_name']);
        echo json_encode($file);
    }
}