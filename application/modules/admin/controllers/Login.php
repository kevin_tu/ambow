<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	public function index()
	{

		if (count($this->post)){
			$this->do_login();
			exit;
		}
		if (isset($_SESSION['admin_user'])){
			header('location:'.site_url() . 'admin');
			exit;
		}
		$this->load->view('admin/login.php', $this->data, FALSE);
	}

	private function do_login(){
		$sql = "select * from manager 
			where userid='{$this->post['userid']}' 
			and userpwd=SHA1(MD5('{$this->post['userpwd']}')) 
			and status=1";
		$query = $this->db->query($sql);

		if ($query->num_rows() == 1){
			$result = array(
				'result'=>'success',
				);

			$user = $query->row_array();

			$_SESSION['admin_user'] = array(
				'userid'=>$user['userid'],
				'auth'=>json_decode($user['auth'])
				);
		}
		else{
			$result = array(
				'result'=>'error'
				);
			unset($_SESSION['admin_user']);
		}
		echo json_encode($result);
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/admin/Login.php */