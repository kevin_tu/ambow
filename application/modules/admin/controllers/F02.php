<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class F02 extends MY_Controller_Admin {

	function __construct(){
		parent::__construct();
		$this->load->model('admin/tags_model');
		$this->func = 'F02';
		$this->data['func'] = $this->func;
		$this->check_auth($this->func);
	}

	public function index()
	{
		$this->data['parent_id'] = isset($this->get['parent_id']) ? $this->get['parent_id'] : 0;
		$this->data['length'] = isset($this->get['length']) ? $this->get['length'] : 20;
		$this->data['start'] = isset($this->get['start']) ? $this->get['start'] : 0;

		$this->data['parent_tag'] = $this->tags_model->get_row(array('id'=>$this->data['parent_id'],'tag_type'=>$this->func));
		// echo json_encode($this->data['parent_tag']);exit;
		
		$this->data['content_view'] = $this->load->view('admin/'.$this->func.'/index', $this->data, true);
		$this->load->view('admin/master_view', $this->data, FALSE);
	}

	public function list_json($parent_id=0){
		$tag_type = $this->func;
		$length = isset($this->post['length']) ? $this->post['length'] : 20;
		$start = isset($this->post['start']) ? $this->post['start'] : 0;

		$search = array();
		if(!empty($_POST['search']['value'])){
			for($i=0 ; $i<count($_POST['columns']);$i++){
				if ($_POST['columns'][$i]['searchable']=='true'){
					$column = $_POST['columns'][$i]['data'];
					$search[$column]=$_POST['search']['value'];
				}
			}
		}

		$rs = $this->tags_model->get_rs($tag_type,$search,$length,$start,$parent_id);
		echo json_encode($rs);
		//print_r($rs);
		//$this->data['rs'] = $rs;
	}

	public function edit($id=0){

		if (count($this->post)){
			$additional = array(
				'img'=>@$this->post['img'],
				'description'=>@$this->post['description'],
				// 'template_data'=>@$this->post['template_data']
				'template_data'=>@$_POST['template_data']
				);
			$tag_data = array(
				'id' => $id,
				'tag_type' => $this->func,
				'status' => @$this->post['status'],
				'title' => @$this->post['title'],
				'parent_id' => @$this->post['parent_id'],
				'additional' => json_encode($additional),
				);

			$tag_id = $this->tags_model->save($tag_data);

			if ($id==0){
				echo "location.replace('" . site_url() . "admin/{$this->func}');";
			}
			exit;
		}
		$search = array('id'=>$id);
		$row = $this->tags_model->get_row(array('id'=>$id,'tag_type'=>$this->func));
		$row['additional'] = json_decode(@$row['additional'],1);

		//樣板模組
		$modal_data = array('list_data'=>array());
		$template_data = is_array($row['additional']['template_data']) ? $row['additional']['template_data'] : array();

		$template_html = array();
		foreach ($template_data as $data) {
			if (isset($data['list_data'])) {
				$new_list_data = array();
				foreach ($data['list_data'] as $value) {
					$value['id'] = rand(100, 999);
					$new_list_data[] = $value;
					$modal_data['list_data'][$value['id']] = $value;
				}
				$data['list_data'] = $new_list_data;
			}
			$html = '<div class="panel panel-default">' . 
				$this->load->view('admin/template/view_' . $data['template_id'], $data, true) . 
				'</div>';
			$template_html[] = $html;
		}

		$this->data['row'] = $row;
		// echo json_encode($modal_data);exit;
		$this->data['modal_data'] = json_encode($modal_data);
		$this->data['template_html'] = implode('', $template_html);
		// echo $this->data['template'];exit;
		$this->data['tags_all'] = $this->tags_model->get_all('F02', array($id));
		$this->data['content_view'] = $this->load->view('admin/'.$this->func.'/edit', $this->data, true);
		$this->load->view('admin/master_view', $this->data, FALSE);

	}

	public function duplicate($tag_id){
		$row = $this->db->where('id', $tag_id)->get('tags')->row_array();
		$row['title'] = $row['title'] . ' - 複製';
		$row['lock'] = 0;
		unset($row['id']);
		// echo json_encode($row);exit;
		$tag_id = $this->tags_model->save($row);
	}


	public function delete($id=0){
		$tag_data = array('id'=>$id);
		$this->tags_model->delete($tag_data);
	}


	public function sort(){
		$tag_id = $this->post['tag_id'];
		$start = $this->post['start'];
		$this->tags_model->sort($this->func,$tag_id,$start);
	}

	public function publish(){
		$tag_id = $this->post['id'];
		$status = $this->post['status'];
		$this->tags_model->publish($tag_id,$status);
	}

}

/* End of file F02.php */
/* Location: ./application/modules/admin/controllers/F02.php */