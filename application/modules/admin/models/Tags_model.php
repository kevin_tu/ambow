<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tags_model extends CI_Model {

	public function get_parents($parent_tag_type){
		$level_1 = $this->db
			->select('x1.*,x2.children')
			->join("(SELECT parent_id,GROUP_CONCAT(id) AS children FROM tags GROUP BY parent_id) x2","x2.parent_id=x1.id","left")
			->where(array('tag_type'=>$parent_tag_type))
			->order_by('sort desc')
			->get('tags x1')->result_array();

		return $level_1;
	}

	public function get_rs_by_parent_id($parent_id){
		$query = $this->db->where(array('parent_id'=>$parent_id))->order_by('sort','desc')->get('tags_view');
		return $query->result_array();
	}

	public function get_rs_selected($post_id){
		$query = $this->db->where(array('post_id'=>$post_id))->get('tag_assoc');
		$result = array();
		foreach($query->result_array() as $row){
			$result[] = $row['tag_id'];
		}
		return $result;
	}

	public function get_all($tag_type, $without_id=array()){
		$this->db->select('id, parent_id, title');
		$this->db->where('tag_type', $tag_type);
		$this->db->where_not_in('id', $without_id);
		$query = $this->db->get('tags');
		$result = array();
		foreach ($query->result_array() as $key => $value) {
			$result[$value['parent_id']][] = $value;
		}
		// echo json_encode($result);exit;
		return $result;
	}

	public function get_rs($tag_type,$search,$length,$start,$parent_id=false){
		$view = "tags";
		$offset = $start;

		if ($parent_id!==false){
			$this->db->where('parent_id',$parent_id);
		}
		$this->db->where('tag_type',$tag_type);
		$query = $this->db->get($view);
		$recordsTotal = $query->num_rows();
		
		if ($parent_id!==false){
			$this->db->where('parent_id',$parent_id);
		}
		$this->db->where('tag_type',$tag_type);
		if (count($search)){
			$this->db->group_start();
			$this->db->or_like($search);
			$this->db->group_end();			
		}
		$query = $this->db->get($view);
		$recordsFiltered = $query->num_rows();
		
		if ($parent_id!==false){
			$this->db->where('parent_id',$parent_id);
		}
		$this->db->where('tag_type',$tag_type);
		if (count($search)){
			$this->db->group_start();
			$this->db->or_like($search);
			$this->db->group_end();			
		}
		$this->db->order_by('sort','desc');
		$query = $this->db->get($view,$length,$offset);

		$response = array(
			"draw" => (int)@$_POST['draw'],
			"recordsTotal" => $recordsTotal,
			"recordsFiltered" => $recordsFiltered,
			"data" => $query->result_array(),
			"sql" => $this->db->last_query()
			);
		return $response;
	}


	public function get_row($param){
		$query = $this->db->where($param)->get('tags');
		$row = $query->row_array();

		if (!empty($row)) {
			$param['id'] = $row['parent_id'];
			$query = $this->db->where($param)->get('tags');
			$row['parent'] = $query->row_array();
		}
		return $row;
	}

	public function save($tag_data){

		$tag_id = $tag_data['id'];


		//posts
		$query = $this->db->get_where('tags',array('id'=>$tag_id));
		if ($query->num_rows()==0){
			$this->db->insert('tags',$tag_data);
			$tag_id = $this->db->insert_id();

			//排序
			$this->db->where('id',$tag_id);
			$this->db->update('tags',array('sort'=>$tag_id));
		}
		else{
			$this->db->where('id',$tag_id);
			$this->db->update('tags',$tag_data);
		}


		return $tag_id;
	}
	public function delete($tag_data){
		$this->db->where_in('id',$tag_data);
		$this->db->delete('tags');

		$this->db->where_in('tag_id',$tag_data);
		$this->db->delete('tag_assoc');
	}
	public function publish($tag_id,$status){
		$this->db->where('id',$tag_id);
		$this->db->update('tags',array('status'=>$status));
	}
	public function sort($tag_type,$tag_id,$start){
		$this->db->select_max('sort');
		$this->db->where('tag_type',$tag_type);
		$query = $this->db->get('tags')->row_array();
		$page_start = $query['sort'] - $start;
		foreach($tag_id as $id){
			//echo $id;
			$data = array('sort'=>$page_start);
			$this->db->where(array('id'=>$id));
			$this->db->update('tags',$data);
			$page_start--;
			//$query = $this->db->get('tag_assoc')->row_array();
			//var_dump($query);
		}
	}


}

/* End of file Tags_model.php */
/* Location: ./application/models/admin/Tags_model.php */