<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }
	public function get_rs($post_type,$tag_id,$search,$length,$start){
		$view = "member";
		$offset = $start;

		$query = $this->db->get($view);
		$recordsTotal = $query->num_rows();
		
		if (count($search)){
			$this->db->group_start();
			$this->db->or_like($search);
			$this->db->group_end();			
		}
		$query = $this->db->get($view);
		$recordsFiltered = $query->num_rows();
		
		if (count($search)){
			$this->db->group_start();
			$this->db->or_like($search);
			$this->db->group_end();			
		}
		$this->db->order_by('createtime','desc');
		$query = $this->db->get($view,$length,$offset);

		$response = array(
			"draw" => (int)@$_POST['draw'],
			"recordsTotal" => $recordsTotal,
			"recordsFiltered" => $recordsFiltered,
			"data" => $query->result_array(),
			"sql" => $this->db->last_query()
			);
		return $response;
	}
	public function get_row($post_type,$search){
		$view = "member";

		$query = $this->db->get_where($view,$search);
		return $query->row_array();

	}
	public function save($post_data,$meta_data,$tag_data){

		$post_id = $post_data['id'];


		//manager
		$query = $this->db->get_where('manager',array('id'=>$post_id));
		if ($query->num_rows()==0){
			$this->db->insert('manager',$post_data);
			$post_id = $this->db->insert_id();
		}
		else{
			$this->db->where('id',$post_id);
			$this->db->update('manager',$post_data);
		}


		return $post_id;
	}
	public function delete($post_data){
		$this->db->where_in('id',$post_data);
		$this->db->delete('manager');
	}
	public function publish($post_id,$status){
		$this->db->where('id',$post_id);
		$this->db->update('posts',array('status'=>$status));
	}
	public function sort($post_type,$post_id,$tag_id,$start){
		$this->db->select_max('sort');
		$query = $this->db->get("{$post_type}_view")->row_array();
		$page_start = $query['sort'] - $start;
		foreach($post_id as $id){
			//echo $id;
			$data = array('sort'=>$page_start);
			$this->db->where(array('post_id'=>$id,'tag_id'=>$tag_id));
			$this->db->update('tag_assoc',$data);
			$page_start--;
			//$query = $this->db->get('tag_assoc')->row_array();
			//var_dump($query);
		}
	}
}

/* End of file Member_model.php */
/* Location: ./application/models/admin/Member_model.php */