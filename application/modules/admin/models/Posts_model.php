<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posts_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }
	public function get_rs($post_type,$tag_id,$search,$length,$start,$order_by='sort desc,id desc'){
		$view = "{$post_type}_view";
		$view = "{$post_type}"; //改抓實體TABLE
		$offset = $start;

		$this->db->where('tag_id',$tag_id);
		$query = $this->db->get($view);
		$recordsTotal = $query->num_rows();
		
		$this->db->where('tag_id',$tag_id);
		if (count($search)){
			$this->db->group_start();
			$this->db->or_like($search);
			$this->db->group_end();			
		}
		$query = $this->db->get($view);
		$recordsFiltered = $query->num_rows();
		
		$this->db->where('tag_id',$tag_id);
		if (count($search)){
			$this->db->group_start();
			$this->db->or_like($search);
			$this->db->group_end();			
		}
		$this->db->order_by($order_by);
		$query = $this->db->get($view,$length,$offset);

		$response = array(
			"draw" => (int)@$_POST['draw'],
			"recordsTotal" => $recordsTotal,
			"recordsFiltered" => $recordsFiltered,
			"data" => $query->result_array(),
			"sql" => $this->db->last_query()
			);
		return $response;
	}
	public function get_row($post_type,$search){
		$view = "{$post_type}_view";
		$view = "{$post_type}"; //改抓實體TABLE

		$query = $this->db->get_where($view,$search);
		return $query->row_array();

	}
	public function save($post_data,$meta_data,$tag_data){

		$post_id = $post_data['id'];


		//posts
		$query = $this->db->get_where('posts',array('id'=>$post_id));
		if ($query->num_rows()==0){
			$this->db->insert('posts',$post_data);
			$post_id = $this->db->insert_id();
		}
		else{
			$this->db->where('id',$post_id);
			$this->db->update('posts',$post_data);
		}

		//meta
		foreach($meta_data as $key=>$item){
			// echo $item['meta_key'];
			$meta_data[$key]['parent_id'] = $post_id;

			if ($item['meta_key']=='post_content') {
				$this->load->helper('simple_html_dom');
				$html = str_get_html($item['meta_value']);
		        if ($html) {
		        	if ($html->find('img')) {
						foreach ($html->find('img') as $key_img=>$img) {
							$img_data = $img->src;
		        			if (strpos($img_data, ';')) {

								//存圖
						        list($type, $img_data) = explode(';', $img_data);
						        list(, $img_data)      = explode(',', $img_data);
						        $img_data = base64_decode($img_data);
						        $filename = date('YmdHis') . rand(1000,9999) .'.png';
						        file_put_contents(FCPATH . 'upload/content_img/' . $filename, $img_data);

						        //替換SRC
						        $html->find('img')[$key_img]->src = site_url() . 'upload/content_img/' . $filename;

						    }

						}
						$meta_data[$key]['meta_value'] = $html;
		        	}
			        	
		        }
			}
		}

		$this->db->where('parent_id',$post_id);
		$this->db->delete('meta');
		$this->db->insert_batch('meta', $meta_data);

		//tags
		foreach($tag_data as $tag_id){
			$query = $this->db->get_where('tag_assoc',array('post_id'=>$post_id,'tag_id'=>$tag_id));
			if ($query->num_rows()==0){
				$sql = "SELECT MAX(sort) as max_sort FROM tag_assoc x1
					INNER JOIN posts x2 ON x2.`id`=x1.`post_id`
					INNER JOIN meta x3 ON x3.`parent_id`=x2.`id` AND x3.`meta_key`='post_startdate' AND x3.`meta_value`<='" . @$this->post['post_startdate'] . "' 
					WHERE x2.`post_type`='{$post_data['post_type']}' AND x1.`tag_id`='{$tag_id}'";

				// echo $sql;exit;
				$row = $this->db->query($sql)->row_array();

				$data = array(
					'post_id'=>$post_id,
					'tag_id'=>$tag_id,
					'sort'=>$row['max_sort']?:$post_id
					);
				// echo json_encode($data);
				$this->db->insert('tag_assoc',$data);
			}
		}
		$this->db->where_not_in('tag_id',$tag_data);
		$this->db->where_not_in('tag_id','0');
		$this->db->where('post_id',$post_id);
		$this->db->delete('tag_assoc');


		return $post_id;
	}
	public function delete($post_data){
		$this->db->where_in('id',$post_data);
		$this->db->delete('posts');

		$this->db->where_in('post_id',$post_data);
		$this->db->delete('tag_assoc');

		$this->db->where_in('parent_id',$post_data);
		$this->db->delete('meta');
	}
	public function publish($post_id,$status){
		$this->db->where('id',$post_id);
		$this->db->update('posts',array('status'=>$status));
	}
	public function sort($post_type,$post_id,$tag_id,$start){
		$this->db->select_max('id');
		$query = $this->db->get("{$post_type}")->row_array();
		$page_start = $query['id'] - $start;
		foreach($post_id as $id){
			//echo $id;
			$data = array('sort'=>$page_start);
			$this->db->where(array('post_id'=>$id,'tag_id'=>$tag_id));
			$this->db->update('tag_assoc',$data);
			$page_start--;
			//$query = $this->db->get('tag_assoc')->row_array();
			//var_dump($query);
		}
	}

}

/* End of file Posts_model.php */
/* Location: ./application/models/Posts_model.php */