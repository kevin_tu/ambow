
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item ">帐号管理</li>
                    </ol>
                </h3>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12 pull-right">
                <p>
                    <a href="<?php echo site_url() . 'admin/'.$func.'/edit' ?>" class="btn btn-default">新增一笔</a><br>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table width="100%" class="table table-responsive display select" cellspacing="0" id="datatable">
                            <thead>
                                <tr>
                                    <th>啟用</th>
                                    <th>帳號</th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<script type="text/javascript">

    $(document).ready(function() {
        var config = {
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "pagingType": "simple_numbers",
            "language":{
                "processing":   "处理中...",
                "loadingRecords": "载入中...",
                "lengthMenu":   "显示 _MENU_ 项结果",
                "zeroRecords":  "没有符合的结果",
                "info":         "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
                "infoEmpty":    "显示第 0 至 0 项结果，共 0 项",
                "infoFiltered": "(从 _MAX_ 项结果中过滤)",
                "infoPostFix":  "",
                "search":       "搜寻:",
                "paginate": {
                    "first":    "第一页",
                    "previous": "上一页",
                    "next":     "下一页",
                    "last":     "最后一页"
                },
                "aria": {
                    "sortAscending":  ": 升幂排列",
                    "sortDescending": ": 降幂排列"
                }
            },
            "columns": [
                {
                    'data': null,
                    'orderable': false,
                    'searchable':false,
                    'className': "center",
                    'render': function ( data, type, row ) {
                        return '<input type="checkbox" name="status" value="'+data.id+'" '+(data.status==1?'checked':'')+'>';
                    },
                    'width': '150px'
                },
                {"data": "userid", 'orderable': false, 'searchable':true},
                {
                    'data': null,
                    'orderable': false,
                    'searchable':false,
                    'className': "center",
                    'render': function ( data, type, row ) {
                        // Combine the first and last names into a single table field
                        return '<a class="" href="<?php echo site_url()?>admin/<?php echo $func ?>/edit/'+data.id+'">编辑</a> <a class=" delete" data-id="'+data.id+'" href="<?php echo site_url()?>admin/<?php echo $func ?>/delete/'+data.id+'">删除</a> ';
                    },
                    'width': '150px'
                    //defaultContent: '<a href="" class="editor_edit" data-id='++'>View</a>'
                }
            ],
            "ajax": {

                url: "<?php echo site_url() . 'admin/' . $func . '/list_json/' ?>",
                type: 'POST'
            },
            "lengthMenu": [[10, 20, 50], [10, 20, 50]],
            "pageLength": parseInt('<?php echo $length ?>'),
            "displayStart": parseInt('<?php echo $start ?>')
        };
        var table = $('#datatable').DataTable(config);

        table.on('draw.dt',function(){ 

            //刪除
            $('.delete').off().on('click',function(e){
                e.preventDefault();
                if (!confirm('确定要删除？')) {return false;}
                var post_id = $(this).data('id');
                $.post('<?php echo site_url()?>admin/<?php echo $func ?>/delete/'+post_id,'',function(){
                    table.draw();  
                })
            })

            //發佈
            $('input[name=status]').off().on('click',function(){
                var q = {};
                q.id = $(this).val();
                q.status = $(this).is(':checked')?1:0;
                console.log(q);
                $.post('<?php echo site_url()?>admin/<?php echo $func ?>/publish',q);
            })




        })
        .on('page.dt',function(){ //換頁
            gen_query_string(table.page.info().start,table.page.info().length,table.search());
        })
        .on('search.dt',function(){
            gen_query_string(table.page.info().start,table.page.info().length,table.search());
        })
        .on('length.dt',function(){
            gen_query_string(table.page.info().start,table.page.info().length,table.search());
        })

        <?php if (strlen(@$_GET['search'])): ?>
        table.search('<?php echo @$_GET["search"] ?>').draw(false);
        <?php endif; ?>
    });

    var gen_query_string = function(start,length,search){
        window.history.pushState(null, null, "?start="+start+"&length="+length+"&search="+search+"");
    }
</script>