
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item ">帐号管理</li>
                    <li class="breadcrumb-item active">编辑</li>
                </ol>
            </h3>

        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <form role="form">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>启用</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="status" id="publish1" value="1" <?php echo @$row['status']==1?'checked':'' ?>>是
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="status" id="publish2" value="0" <?php echo @$row['status']!=1?'checked':'' ?>>否
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>帐号</label>
                                    <input class="form-control" name="userid" value="<?php echo @$row['userid'] ?>">
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group">
                                    <label>密码</label>
                                    <input type="password" class="form-control" name="pwd" value="">
                                    <p class="help-block">使用旧密码请保持空白</p>
                                </div>
                                <div class="form-group">
                                    <label>密码确认</label>
                                    <input type="password" class="form-control" name="pwd_confirm" value="">
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <!-- /.col-md-4 (nested) -->
                            <div class="col-md-4">
                                <label>权限</label>
                                

                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" class="select_all">全部勾选
                                    </label>
                                    <p class="help-block"></p>
                                </div>
                                <div class="auth_panel">
                                    
                                <?php foreach ($menu_all as $level_1): ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="checkbox-inline">
                                                <?php //if (count($level_1['children'])==0): ?>
                                                    <input type="checkbox" value="<?php echo $level_1['sn'] ?>" <?php echo in_array($level_1['sn'],$row['auth'])?'checked':'' ?>>
                                                <?php //endif ?>
                                                <?php echo $level_1['name'] ?>
                                            </label>
                                        </div>
                                    </div>
                                    <?php foreach ($level_1['children'] as $level_2): ?>
                                        
                                        <div class="row">
                                            <div class="col-md-1"></div>
                                            <div class="col-md-11">
                                                <label class="checkbox-inline">
                                                    <?php //if (count($level_2['children'])==0): ?>
                                                        <input type="checkbox" class="<?php echo $level_1['sn'] ?>" value="<?php echo $level_2['sn'] ?>" <?php echo in_array($level_2['sn'],$row['auth'])?'checked':'' ?>>
                                                    <?php //endif ?>
                                                    <?php echo $level_2['name'] ?>
                                                </label>
                                            </div>
                                        </div>
                                        <?php foreach ($level_2['children'] as $level_3): ?>
                                            
                                            <div class="row">
                                                <div class="col-md-2"></div>
                                                <div class="col-md-10">
                                                    <label class="checkbox-inline">
                                                        <?php //if (count($level_3['children'])==0): ?>
                                                            <input type="checkbox" class="<?php echo $level_2['sn'] ?>" value="<?php echo $level_3['sn'] ?>" <?php echo in_array($level_3['sn'],$row['auth'])?'checked':'' ?>>
                                                        <?php //endif ?>
                                                        <?php echo $level_3['name'] ?>
                                                    </label>
                                                </div>
                                            </div>
                                            <?php foreach ($level_3['children'] as $level_4): ?>
                                                <div class="row">
                                                    <div class="col-md-3"></div>
                                                    <div class="col-md-9">
                                                        <label class="checkbox-inline">
                                                            <input type="checkbox" class="<?php echo $level_3['sn'] ?>" value="<?php echo $level_4['sn'] ?>" <?php echo in_array($level_4['sn'],$row['auth'])?'checked':'' ?>>
                                                            <?php echo $level_4['name'] ?>
                                                        </label>
                                                    </div>
                                                </div>                                                
                                            <?php endforeach ?>
                                        <?php endforeach ?>
                                    <?php endforeach ?>
                                <?php endforeach ?>
                                </div>
                                        
                                    
                            </div>
                            <!-- /.col-md-4 (nested) -->
                            <p></p>
                        </div>
                        <!-- /.row (nested) -->
                        <div class="row">
                            <div class="col-lg-12">
                                <a href="<?php echo site_url().'admin/' . $func . '/' ?>" class="btn btn-default">回列表</a>
                                <button type="submit" class="btn btn-default" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">储存</button>
                            </div>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </form>
</div>
<script type="text/javascript">

    $(document).ready(function() {

        $('form').on('submit',function(e){
            e.preventDefault();

            var q = {}
            q.id = "<?php echo @$row['id'] ?>";
            q.status = $('input[name=status]:checked').val();
            q.userid = $('input[name=userid]').val();
            q.userpwd = $('input[name=pwd]').val();

            q.auth = $('.auth_panel input[type=checkbox]:checked').map(function(){
                        return this.value;
                    }).get();


            if (q.userid==''){
                alert('请输入帐号');
                return false;
            }
            if (q.id==''){
                if (q.userpwd==''){
                    alert("请输入密码");
                    return false;
                }
                if (q.userpwd != $('input[name=pwd_confirm]').val()){
                    alert("两组密码不相同");
                    return false;
                }
            }
            else{
                if (q.userpwd != '' || $('input[name=pwd_confirm]').val() != ''){
                    if (q.userpwd == ''){

                        alert("请输入密码");
                        return false;
                    }
                    if (q.userpwd != $('input[name=pwd_confirm]').val()){
                        alert("两组密码不相同");
                        return false;
                    }
                }
            }

            //console.log(q); return false;
            $('button[type=submit]').button('loading');
            $.post('',q,function(data){
                $('button[type=submit]').button('reset');
            },'script');
        })
        
        $('input[type=checkbox].select_all').on('change',function(){
            if ($(this).prop('checked')){
                $('.auth_panel input[type=checkbox]').prop('checked',true)
            }
        })
        $('.auth_panel input[type=checkbox]').on('change',function(){
            var classname = $(this).attr('class');
            console.log('change',$(this).prop('checked'));
            if (!$(this).prop('checked')){
                $('input[type=checkbox].select_all').prop('checked',false)

                if ($('.auth_panel input[type=checkbox].'+classname+':checked').length==0){
                    $('.auth_panel input[value="'+classname+'"]').prop('checked',false);
                }
            }
            else{
                $('.auth_panel input[type=checkbox].'+$(this).val()).prop('checked',true)
                $('.auth_panel input[value="'+classname+'"]').prop('checked',true);
            }

        })

        if ($('.auth_panel input[type=checkbox]:not(:checked)').length==0){
            $('input[type=checkbox].select_all').prop('checked',true)
        }
    });
</script>