
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item ">安博资讯</li>
                    <li class="breadcrumb-item ">文章</li>
                    <li class="breadcrumb-item active">编辑</li>
                </ol>
            </h3>

        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <form role="form">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>前台显示 </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="status" id="publish1" value="1" <?php echo @$row['status']==1?'checked':'' ?>>是
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="status" id="publish2" value="0" <?php echo @$row['status']!=1?'checked':'' ?>>否
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>置顶</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="post_is_top" id="top1" value="1" <?php echo @$row['post_is_top']==1?'checked':'' ?>>是
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="post_is_top" id="top2" value="0" <?php echo @$row['post_is_top']!=1?'checked':'' ?>>否
                                    </label>
                                </div>
                                <div class="form-group ">
                                    <label>语系</label>
                                    <select name="tag[]">
                                        <!-- <option value="0">主類別</option> -->
                                        <?php foreach (@$level_1[0] as $tag): ?>
                                            <?php echo '<option value="' . $tag['id'] . '" ' . (in_array($tag['id'],$row['tags_selected'])?'selected':'') . ' >' . $tag['title'] . '</option>' ?>
                                        <?php endforeach ?>
                                    </select>
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group">
                                    <label>标题</label>
                                    <input class="form-control" name="post_title" value="<?php echo @$row['post_title'] ?>">
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group">
                                    <label>发布日期</label>
                                    <input class="form-control date" name="post_date" value="<?php echo date('Y-m-d',strtotime(@$row['createtime'])) ?>">
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group">
                                    <label>封面图片(1050x520)</label>
                                    <input type="hidden" name="post_cover" id="post_cover" data-size="1050x520" data-title="" data-info="" value="<?php echo @$row['post_cover'] ?>" >
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>类别</label>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <?php foreach ($category as $tag): ?>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label class="checkbox-inline">
                                                            <input name="tag[]" type="radio" value="<?php echo $tag['id'] ?>" <?php echo in_array($tag['id'],$row['tags_selected'])?'checked':'' ?>>
                                                            <?php echo $tag['title'] ?>
                                                        </label>
                                                    </div>
                                                </div>
                                            <?php endforeach ?>
                                        </div>

                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>微信</label>
                                    <input class="form-control " name="post_url_wechat" value="<?php echo @$row['post_url_wechat'] ?>">
                                    <p class="help-block"></p>
                                    <label>百家号</label>
                                    <input class="form-control " name="post_url_baijiahao" value="<?php echo @$row['post_url_baijiahao'] ?>">
                                    <p class="help-block"></p>
                                    <label>新浪博客</label>
                                    <input class="form-control " name="post_url_sina" value="<?php echo @$row['post_url_sina'] ?>">
                                    <p class="help-block"></p>
                                    <label>新浪微博</label>
                                    <input class="form-control " name="post_url_weibo" value="<?php echo @$row['post_url_weibo'] ?>">
                                    <p class="help-block"></p>
                                    <label>企鹅号</label>
                                    <input class="form-control " name="post_url_qq" value="<?php echo @$row['post_url_qq'] ?>">
                                    <p class="help-block"></p>
                                    <label>今日头条</label>
                                    <input class="form-control " name="post_url_toutiao" value="<?php echo @$row['post_url_toutiao'] ?>">
                                    <p class="help-block"></p>
                                    <label>搜狐自媒体</label>
                                    <input class="form-control " name="post_url_sohu" value="<?php echo @$row['post_url_sohu'] ?>">
                                    <p class="help-block"></p>
                                </div>
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>图文模组</label>                                    
                                    <div class="panel-body">
                                        <div class="panel-group" id="accordion">
                                            <?php echo @$template_html; ?>
                                        </div>
                                        <div>
                                            <select class="template_id">
                                                <option value="01">单一图</option>
                                                <option value="02">单栏文字</option>
                                                <option value="03">左图、右文字</option>
                                                <option value="04">左文字、右图</option>
                                            </select>
                                            <a href="#" class="add_template">新增样板</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- /.row (nested) -->
                        <div class="row">
                            <div class="col-lg-12">
                                <a href="<?php echo site_url().'admin/'.$func.'/' ?>" class="btn btn-default">回列表</a>
                                <button type="submit" class="btn btn-default" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">储存</button>
                                <?php if (@$row['id']!=''): ?>
                                <!-- <a href="<?php echo site_url().'admin/' . $func . '/preview/' . $row['id'] ?>" class="btn btn-default" target="_blank">預覽</a> -->
                                <?php endif ?>
                            </div>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </form>
</div>
<style type="text/css">
    .myGalleryUpload_item input[type=text]{display:none;}
</style>
<script type="text/javascript">

    $(document).ready(function() {

        $('#post_cover').myCoverUpload({
            img_url: '<?PHP echo site_url() . "upload/"?>',
            upload_url: '<?PHP echo site_url() . "admin/upload/crop_upload"?>',
            crop_url: '<?PHP echo site_url() . "admin/upload/crop"?>',
            thumbnail_type: 'crop',
            thumbnail_size: [$('#post_cover').attr('data-size')]
        });

        $('#gallery').myGalleryUpload({
            img_url: '<?PHP echo site_url() . "upload/"?>',
            upload_url: '<?PHP echo site_url() . "admin/upload/gallery_upload"?>',
            crop_url: '<?PHP echo site_url() . "admin/upload/crop"?>',
            thumbnail_type: 'crop',
            // thumbnail_size: ['610x0']
        });

        $('.date').datepicker({ 
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            maxDate: '0'
        })

        $('.summernote_simple').summernote({
            height:300,
            toolbar:[
                ['insert',['link']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['codeview',['codeview']]
            ],
            onKeyup: function(e) {
                $(this).val($(this).code());
            },
        });

        $('.summernote').summernote({
            prettifyHtml:true,
            toolbar: [
                // [groupName, [list of button]]
                // ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['fontsize', 'color']],
                // ['font', ['fontname']],
                ['para', ['paragraph']],
                ['insert', ['link','picture', 'doc', 'video']], // image and doc are customized buttons
                ['misc', ['codeview', 'fullscreen']],
            ],
            fontSizes: ['12', '15', '17'],
            callbacks: {
                onPaste: function (e) {
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                    e.preventDefault();
                    document.execCommand('insertText', false, bufferText);
                }
            }
        });
        $('.note-editable').css('font-size','15px');
        $('.note-current-fontsize').text('15');

        $('form').on('submit',function(e){
            e.preventDefault();

            var template_data = [];
            $('#accordion>.panel').each(function(){
                var data = {};
                data.template_id = $(this).find('input[name=template_id]').val();
                data.option = $(this).find('input[type="radio"]:checked').map(function(){
                        return this.value;
                    }).get();
                data.img = $(this).find('input.img').map(function(){
                        return this.value;
                    }).get();
                data.txt = $(this).find('input[name=txt],textarea[name=template_txt]').map(function(){
                        return this.value;
                    }).get();
                data.video = $(this).find('input[name=template_video],textarea[name=template_video]').map(function(){
                        return this.value;
                    }).get();

                template_data.push(data);
            });

            var q = {}
            q.id = "<?php echo @$row['id'] ?>";
            q.status = $('input[name=status]:checked').val();
            q.post_title = $('input[name=post_title]').val();
            q.post_date = $('input[name=post_date]').val();
            q.post_cover = $('input[name=post_cover]').val();
            q.post_content = $('textarea[name=post_content]').val();
            q.post_template = template_data;
            q.post_is_top = $('input[name=post_is_top]:checked').val();
            q.post_url_wechat = $('input[name=post_url_wechat]').val();
            q.post_url_baijiahao = $('input[name=post_url_baijiahao]').val();
            q.post_url_sina = $('input[name=post_url_sina]').val();
            q.post_url_weibo = $('input[name=post_url_weibo]').val();
            q.post_url_qq = $('input[name=post_url_qq]').val();
            q.post_url_toutiao = $('input[name=post_url_toutiao]').val();
            q.post_url_sohu = $('input[name=post_url_sohu]').val();
            
            q.gallery = $.map( $('input[name="gallery[]"]'), function( n, i ) {
                return ( $(n).val() );
            });
            q.tags = $('select[name="tag[]"] option:checked, input[name="tag[]"]:checked').map(function(){
                        return this.value;
                    }).get();

            // console.log(q); return false;
            $('button[type=submit]').button('loading');
            $.post('',q,function(){
                $('button[type=submit]').button('reset');
                alert('储存完成',  '',  'success');
            },'script');
        })


        $('a.add_template').on('click',function(e){
            e.preventDefault();
            var template_id = $(this).prev().val();
            var template = $('<div class="panel panel-default"></div>')
            template.load('<?php echo site_url() ?>admin/template/load/' + template_id,function(data){
                $('#accordion').append(template);
                template.find('input.img').each(function(){
                    var instance = $(this);
                    instance.myCoverUpload({
                        img_url: '<?PHP echo site_url() . "upload/"?>',
                        upload_url: '<?PHP echo site_url() . "admin/upload/image_upload"?>',
                        thumbnail_size: [instance.attr('data-size')]
                    });

                })
                template.find('a.remove_template').each(function(){
                    var instance = $(this);
                    instance.on('click',function(e){
                        e.preventDefault();
                        console.log(instance.parents('.panel:first').remove());
                    })
                })


                template.find('textarea.summer').summernote({
                    toolbar:[
                        ['insert',['link']],
                        ['font', ['bold', 'italic', 'underline', 'clear']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                    ],
                    onKeyup: function(e) {
                        $(this).val($(this).code());
                    },
                });

                template.find('.modal .save').on('click', function(){
                    var instance = $(this);
                    var instance_id = instance.data('id')
                    var list_item = {};
                    var is_new_item = true;
                    if (template.find('input[name=list_id]').val()) {
                        list_item.id = template.find('input[name=list_id]').val();
                        is_new_item = false;
                    }
                    else{
                        list_item.id = Math.floor(Math.random()*(99-10+1))+10;
                    }
                    // console.log(list_item.id)
                    list_item.title = template.find('textarea[name=txt1]').val();
                    list_item.content = template.find('textarea[name=txt2]').val();
                    modal_data.list_data[list_item.id] = list_item;
                    // console.log(modal_data);
                    template.find('.modal').modal('hide');


                    var li = $('<li class="list-group-item" id="list_' + list_item.id + '"></li>');
                    var a = $('<span class="fa fa-arrows"></span> <a href="#" data-id="' + list_item.id + '">' + nl2br(list_item.title) + '</a>');
                    li.append(a);
                    if (is_new_item) {
                        template.find('.list-group').append(li);
                    }
                    else{
                        $('li#list_'+list_item.id).find('a').text(list_item.title)
                    }

                    template.find('.list-group a').off().on('click', function(e){
                        e.preventDefault();
                        console.log(e);
                        var id = $(this).data('id');
                        list_item = modal_data.list_data[id];
                        template.find('input[name=list_id]').val(list_item.id);
                        template.find('textarea[name=txt1]').val(list_item.title);
                        template.find('textarea[name=txt2]').summernote("code", list_item.content);
                        template.find('.list_modal').modal('show');
                    })

                    template.find('.list-group').sortable({ 
                        handle: "span.fa-arrows"
                    });
                })

                template.find('.modal .delete').on('click', function(){
                    var id = template.find('.modal input[name=list_id]').val();
                    // modal_data.list_data.splice(id, 1);
                    console.log(modal_data);
                    $('li#list_'+id).remove();

                })

                template.find('.modal').on('hide.bs.modal', function (event) {
                    // console.log('modal hide');
                    template.find('input[name=list_id]').val('');
                    template.find('textarea[name=txt1]').val('');
                    template.find('textarea[name=txt2]').summernote("code", '<p><br /></p>');
                })



            });
        })


        $('#accordion .panel').find('input.img').each(function(){
            var instance = $(this);
            instance.myCoverUpload({
                img_url: '<?PHP echo site_url() . "upload/"?>',
                upload_url: '<?PHP echo site_url() . "admin/upload/image_upload"?>',
                thumbnail_size: [instance.attr('data-size')]
            });

        })
        $('#accordion .panel').find('a.remove_template').each(function(){
            var instance = $(this);
            instance.on('click',function(e){
                e.preventDefault();
                console.log(instance.parents('.panel:first').remove());
            })
        })
        $('#accordion .panel').find('textarea.summer').summernote({
            toolbar:[
                ['insert',['link']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
            ],
            onKeyup: function(e) {
                $(this).val($(this).code());
            },
        });

        $('#accordion.panel-group').sortable({ 
            handle: ".panel-heading .fa-arrows"
        });

    });
</script>