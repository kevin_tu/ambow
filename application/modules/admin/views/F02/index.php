
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item ">一般页面</li>
                        <?php if (!empty($parent_tag)): ?>
                            <?php if (!empty($parent_tag['parent'])): ?>
                            <li class="breadcrumb-item ">
                                <a href="<?php echo site_url('/admin/' . $func . '/?parent_id=' . $parent_tag['parent_id']) ?>"><?php echo $parent_tag['parent']['title'] ?></a>
                            </li>
                            <?php endif ?>
                            <li class="breadcrumb-item "><?php echo @$parent_tag['title'] ?></li>
                        <?php endif ?>
                    </ol>
                </h3>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12 pull-right">
                <p>
                    <a href="<?php echo site_url() . 'admin/'.$func.'/edit' ?>" class="btn btn-default">新增一笔</a><br>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table width="100%" class="table table-responsive display select" cellspacing="0" id="datatable">
                            <thead>
                                <tr>
                                    <th>前台显示</th>
                                    <th>标题</th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<script type="text/javascript">

    $(document).ready(function() {
        var config = {
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "pagingType": "simple_numbers",
            "language":{
                "processing":   "处理中...",
                "loadingRecords": "载入中...",
                "lengthMenu":   "显示 _MENU_ 项结果",
                "zeroRecords":  "没有符合的结果",
                "info":         "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
                "infoEmpty":    "显示第 0 至 0 项结果，共 0 项",
                "infoFiltered": "(从 _MAX_ 项结果中过滤)",
                "infoPostFix":  "",
                "search":       "搜寻:",
                "paginate": {
                    "first":    "第一页",
                    "previous": "上一页",
                    "next":     "下一页",
                    "last":     "最后一页"
                },
                "aria": {
                    "sortAscending":  ": 升幂排列",
                    "sortDescending": ": 降幂排列"
                }
            },
            "columns": [
                {
                    'data': null,
                    'orderable': false,
                    'searchable':false,
                    'className': "center",
                    'render': function ( data, type, row ) {
                        return '<input type="checkbox" name="status" value="'+data.id+'" '+(data.status==1?'checked':'')+'>';
                    },
                    'width': '150px'
                },
                {"data": "title", 'orderable': false, 'searchable':true},
                {
                    'data': null,
                    'orderable': false,
                    'searchable':false,
                    'className': "center",
                    'render': function ( data, type, row ) {
                        // Combine the first and last names into a single table field
                        var btn = '';
                        if (data.parent_id>0){
                            btn += '<a class="" href="<?php echo site_url() . 'admin/' . $func ?>/edit/'+data.id+'">编辑</a>' +
                                ' <a class=" duplicate" data-id="'+data.id+'" href="<?php echo site_url()?>admin/<?php echo $func ?>/duplicate/'+data.id+'">复制</a> ';
                        }
                        btn += ' <a class="" href="<?php echo site_url() . 'admin/' . $func ?>/?parent_id='+data.id+'">子页面</a>';
                        if (data.lock!='1'){
                            btn += ' <a class=" delete" data-id="'+data.id+'" href="<?php echo site_url() . 'admin/' . $func ?>/delete/'+data.id+'">删除</a>';
                        }
                        return btn;
                    },
                    'width': '200px'
                    //defaultContent: '<a href="" class="editor_edit" data-id='++'>View</a>'
                }
            ],
            "ajax": {

                url: "<?php echo site_url() . 'admin/'.$func.'/list_json/' . $parent_id?>",
                type: 'POST'
            },
            "lengthMenu": [[20, 50, 100], [20, 50, 100]],
            "pageLength": parseInt('<?php echo $length ?>'),
            "displayStart": parseInt('<?php echo $start ?>')
        };
        var table = $('#datatable').DataTable(config);

        table.on('draw.dt',function(){ 
            //排序
            $('tbody').sortable({ 
                //handle: ".fa-arrows",
                helper:function(e,ui){
                    ui.children().each(function() {
                        $(this).width($(this).width());
                        $(this).height($(this).height());
                    });
                    return ui;
                },
                start: function (event, ui) {
                    
                },
                stop: function (event, ui) {
                    var tag_id = [];
                    $('input[name=status]').each(function(){
                        tag_id.push($(this).val());
                    })
                    var q = {};
                    q.tag_id = tag_id;
                    q.start = table.page.info().start;
                    $.post('<?php echo site_url()?>admin/<?php echo $func ?>/sort/',q);
                }
            });

            //刪除
            $('.delete').off().on('click',function(e){
                e.preventDefault();
                if (!confirm('确定要删除？')) return false;
                var tag_id = $(this).data('id');
                $.post('<?php echo site_url()?>admin/<?php echo $func ?>/delete/'+tag_id,'',function(){
                    table.draw();  
                })
            })

            //發佈
            $('input[name=status]').off().on('click',function(){
                var q = {};
                q.id = $(this).val();
                q.status = $(this).is(':checked')?1:0;
                console.log(q);
                $.post('<?php echo site_url()?>admin/<?php echo $func ?>/publish',q);
            })

            //複製
            $('.duplicate').off().on('click',function(e){
                e.preventDefault();
                // if (!confirm('確定要刪除？')) {return false;}
                var tag_id = $(this).data('id');
                $.post('<?php echo site_url()?>admin/<?php echo $func ?>/duplicate/'+tag_id,'',function(){
                    table.draw();  
                })
            })




        })
        .on('page.dt',function(){ //換頁
            gen_query_string(table.page.info().start,table.page.info().length,table.search());
        })
        .on('search.dt',function(){
            gen_query_string(table.page.info().start,table.page.info().length,table.search());
        })
        .on('length.dt',function(){
            gen_query_string(table.page.info().start,table.page.info().length,table.search());
        })

        <?php if (strlen(@$_GET['search'])): ?>
        table.search('<?php echo @$_GET["search"] ?>').draw(false);
        <?php endif; ?>
    });

    var gen_query_string = function(start,length,search){
        window.history.pushState(null, null, "?parent_id=<?php echo $parent_id?>start="+start+"&length="+length+"&search="+search+""+"&parent_id="+search+"");
    }
</script>