
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                <ol class="breadcrumb"> 
                    <li class="breadcrumb-item ">一般页面</li>
                    <li class="breadcrumb-item active">编辑</li>
                </ol>
            </h3>

        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <form role="form">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>前台显示</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="status" id="publish1" value="1" <?php echo @$row['status']==1?'checked':'' ?>>是
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="status" id="publish2" value="0" <?php echo @$row['status']!=1?'checked':'' ?>>否
                                    </label>
                                </div>
                                <!-- <div class="form-group">
                                    <label>使用外部連結</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="is_link" id="is_link1" value="1" <?php echo @$row['additional']['is_link']==1?'checked':'' ?>>是
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="is_link" id="is_link2" value="0" <?php echo @$row['additional']['is_link']!=1?'checked':'' ?>>否
                                    </label>
                                </div>
                                <div class="form-group outer_url">
                                    <label>外部連結</label>
                                    <input class="form-control" name="url" value="<?php echo @$row['additional']['url'] ?>">
                                    <p class="help-block"></p>
                                </div> -->
                                <div class="form-group ">
                                    <label>标题</label>
                                    <input class="form-control" name="title" value="<?php echo @$row['title'] ?>">
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group ">
                                    <label>层级</label>
                                    <select name="parent_id">
                                        <!-- <option value="0">主類別</option> -->
                                        <?php foreach (@$tags_all[0] as $tag): ?>
                                            <?php echo '<option value="' . $tag['id'] . '" ' . ($tag['id']==@$row['parent_id']?'selected':'') . '>' . $tag['title'] . '子页面</option>' ?>
                                            <?php foreach (@$tags_all[1] as $tag2): ?>
                                                <?php if ($tag2['parent_id']==$tag['id']): ?>
                                                    <?php echo '<option value="' . $tag2['id'] . '" ' . ($tag2['id']==@$row['parent_id']?'selected':'') . '>' . $tag2['title'] . '子页面</option>' ?>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endforeach ?>
                                    </select>
                                    <p class="help-block"></p>
                                </div>

                                <!-- <div class="form-group">
                                    <label>類別描述</label>
                                    <textarea class="form-control summernote" id="description" rows="3" name="description"><?php echo @$row['additional']['description'] ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label>圖片</label>
                                    <input type="hidden" class="img" id="img" data-size="1920x1280" data-title="" data-info="" value="<?php echo @$row['additional']['img'] ?>" >
                                    <p class="help-block"></p>
                                </div> -->
                                
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>图文模组</label>                                    
                                    <div class="panel-body">
                                        <div class="panel-group" id="accordion">
                                            <?php echo @$template_html; ?>
                                        </div>
                                        <div>
                                            <select class="template_id">
                                                <option value="01">单一图</option>
                                                <option value="02">单栏文字</option>
                                                <option value="03">左图、右文字</option>
                                                <option value="04">左文字、右图</option>
                                                <option value="06">清单</option>
                                            </select>
                                            <a href="#" class="add_template">新增样板</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- /.row (nested) -->
                        <div class="row">
                            <div class="col-lg-12">
                                <a href="<?php echo site_url().'admin/'.$func.'/' ?>" class="btn btn-default">回列表</a>
                                <button type="submit" class="btn btn-default" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">储存</button>
                            </div>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </form>
</div>
<script type="text/javascript">

    var modal_data = <?php echo $modal_data ?>;

    $(document).ready(function() {

        $('#img').myCoverUpload({
            img_url: '<?PHP echo site_url() . "upload/"?>',
            upload_url: '<?PHP echo site_url() . "admin/upload/image_upload"?>',
            //crop_url: '<?PHP echo site_url() . "admin/upload/crop"?>',
            thumbnail_type: '',
            thumbnail_size: [$('#img').attr('data-size')]
        });

        $('.summernote').summernote({
            height:300,
            toolbar:[
                ['insert',['link']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['codeview',['codeview']]
            ],
            onKeyup: function(e) {
                $(this).val($(this).code());
            },
        });

        $('form').on('submit',function(e){
            e.preventDefault();


            var template_data = [];
            $('#accordion>.panel').each(function(){
                var data = {};
                data.template_id = $(this).find('input[name=template_id]').val();
                data.option = $(this).find('input[type="radio"]:checked').map(function(){
                        return this.value;
                    }).get();
                data.img = $(this).find('input.img').map(function(){
                        return this.value;
                    }).get();
                data.txt = $(this).find('input[name=txt],textarea[name=template_txt]').map(function(){
                        return this.value;
                    }).get();
                data.video = $(this).find('input[name=template_video],textarea[name=template_video]').map(function(){
                        return this.value;
                    }).get();

                data.list_data = $(this).find('li.list-group-item a').map(function(){
                        return modal_data.list_data[$(this).data('id')];
                    }).get();

                template_data.push(data);
            });

            // console.log(template_data);return false;


            var q = {}
            q.id = "<?php echo @$row['id'] ?>";
            q.status = $('input[name=status]:checked').val();
            q.title = $('input[name=title]').val();
            q.parent_id = $('select[name=parent_id]').val();
            q.img = $('#img').val();
            q.description = $('#description').val();
            q.template_data = template_data

            $('button[type=submit]').button('loading');
            $.post('',q,function(){
                $('button[type=submit]').button('reset');
                alert('儲存完成',  '',  'success');
            },'script');
        })

        $('input[name=is_link]').on('change',function(){
            if ($('input[name=is_link]:checked').val()==1) {
                $('.outer_url').show();
            }
            else{
                $('.outer_url').hide();
            }
        }).trigger('change');


        
        $('a.add_template').on('click',function(e){
            e.preventDefault();
            var template_id = $(this).prev().val();
            var template = $('<div class="panel panel-default"></div>')
            template.load('<?php echo site_url() ?>admin/template/load/' + template_id,function(data){
                $('#accordion').append(template);
                template.find('input.img').each(function(){
                    var instance = $(this);
                    instance.myCoverUpload({
                        img_url: '<?PHP echo site_url() . "upload/"?>',
                        upload_url: '<?PHP echo site_url() . "admin/upload/image_upload"?>',
                        thumbnail_size: [instance.attr('data-size')]
                    });

                })
                template.find('a.remove_template').each(function(){
                    var instance = $(this);
                    instance.on('click',function(e){
                        e.preventDefault();
                        console.log(instance.parents('.panel:first').remove());
                    })
                })


                template.find('textarea.summer').summernote({
                    toolbar:[
                        ['insert',['link']],
                        ['font', ['bold', 'italic', 'underline', 'clear']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                    ],
                    onKeyup: function(e) {
                        $(this).val($(this).code());
                    },
                });

                template.find('.modal .save').on('click', function(){
                    var instance = $(this);
                    var instance_id = instance.data('id')
                    var list_item = {};
                    var is_new_item = true;
                    if (template.find('input[name=list_id]').val()) {
                        list_item.id = template.find('input[name=list_id]').val();
                        is_new_item = false;
                    }
                    else{
                        list_item.id = Math.floor(Math.random()*(99-10+1))+10;
                    }
                    // console.log(list_item.id)
                    list_item.title = template.find('textarea[name=txt1]').val();
                    list_item.subtitle = template.find('textarea[name=txt3]').val();
                    list_item.content = template.find('textarea[name=txt2]').val();
                    if (typeof modal_data.list_data[list_item.id]=='undefined') {
                        
                    }
                    modal_data.list_data[list_item.id] = list_item;
                    // console.log(modal_data);
                    template.find('.modal').modal('hide');


                    var li = $('<li class="list-group-item" id="list_' + list_item.id + '"></li>');
                    var a = $('<span class="fa fa-arrows"></span> <a href="#" data-id="' + list_item.id + '">' + nl2br(list_item.title) + '</a>');
                    li.append(a);
                    if (is_new_item) {
                        template.find('.list-group').append(li);
                    }
                    else{
                        $('li#list_'+list_item.id).find('a').text(list_item.title)
                    }

                    template.find('.list-group a').off().on('click', function(e){
                        e.preventDefault();
                        console.log(e);
                        var id = $(this).data('id');
                        list_item = modal_data.list_data[id];
                        template.find('input[name=list_id]').val(list_item.id);
                        template.find('textarea[name=txt1]').val(list_item.title);
                        template.find('textarea[name=txt3]').val(list_item.subtitle);
                        template.find('textarea[name=txt2]').summernote("code", list_item.content);
                        template.find('.list_modal').modal('show');
                    })

                    template.find('.list-group').sortable({ 
                        handle: "span.fa-arrows"
                    });
                })

                template.find('.modal .delete').on('click', function(){
                    var id = template.find('.modal input[name=list_id]').val();
                    // modal_data.list_data.splice(id, 1);
                    console.log(modal_data);
                    $('li#list_'+id).remove();

                })

                template.find('.modal').on('hide.bs.modal', function (event) {
                    // console.log('modal hide');
                    template.find('input[name=list_id]').val('');
                    template.find('textarea[name=txt1]').val('');
                    template.find('textarea[name=txt3]').val('');
                    template.find('textarea[name=txt2]').summernote("code", '<p><br /></p>');
                })



            });
        })




        $('#accordion .panel').find('input.img').each(function(){
            var instance = $(this);
            instance.myCoverUpload({
                img_url: '<?PHP echo site_url() . "upload/"?>',
                upload_url: '<?PHP echo site_url() . "admin/upload/image_upload"?>',
                thumbnail_size: [instance.attr('data-size')]
            });

        })
        $('#accordion .panel').find('a.remove_template').each(function(){
            var instance = $(this);
            instance.on('click',function(e){
                e.preventDefault();
                console.log(instance.parents('.panel:first').remove());
            })
        })
        $('#accordion .panel').find('textarea.summer').summernote({
            toolbar:[
                ['insert',['link']],
                ['font', ['style', 'bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
            ],
            onKeyup: function(e) {
                $(this).val($(this).code());
            },
        });

        $('#accordion .panel').find('.modal .save').on('click', function(){
            var instance = $(this);
            var template = $('#accordion .panel').find('.modal .save').parents('.panel:eq(0)');
            var instance_id = instance.data('id')
            var list_item = {};
            var is_new_item = true;
            if (template.find('input[name=list_id]').val()) {
                list_item.id = template.find('input[name=list_id]').val();
                is_new_item = false;
            }
            else{
                list_item.id = Math.floor(Math.random()*(99-10+1))+10;
            }
            // console.log(list_item.id)
            list_item.title = template.find('textarea[name=txt1]').val();
            list_item.subtitle = template.find('textarea[name=txt3]').val();
            list_item.content = template.find('textarea[name=txt2]').val();
            modal_data.list_data[list_item.id] = list_item;
            // console.log(modal_data);
            template.find('.modal').modal('hide');


            var li = $('<li class="list-group-item" id="list_' + list_item.id + '"></li>');
            var a = $('<span class="fa fa-arrows"></span> <a href="#" data-id="' + list_item.id + '">' + nl2br(list_item.title) + '</a>');
            li.append(a);
            if (is_new_item) {
                template.find('.list-group').append(li);
            }
            else{
                $('li#list_'+list_item.id).find('a').text(list_item.title)
            }

            console.log(template.find('.list-group a'))

            template.find('.list-group a').off().on('click', function(e){
                e.preventDefault();
                console.log(e);
                var id = $(this).data('id');
                list_item = modal_data.list_data[id];
                template.find('input[name=list_id]').val(list_item.id);
                template.find('textarea[name=txt1]').val(list_item.title);
                template.find('textarea[name=txt3]').val(list_item.subtitle);
                template.find('textarea[name=txt2]').summernote("code", list_item.content);
                template.find('.list_modal').modal('show');
            })

            template.find('.list-group').sortable({ 
                handle: "span.fa-arrows"
            });
        })

        $('#accordion .panel').find('.list-group a').off().on('click', function(e){
            e.preventDefault();
            var instance = $(this);
            var template = $('#accordion .panel').find('.modal .save').parents('.panel:eq(0)');
            var id = $(this).data('id');
            list_item = modal_data.list_data[id];
            template.find('input[name=list_id]').val(list_item.id);
            template.find('textarea[name=txt1]').val(list_item.title);
            template.find('textarea[name=txt3]').val(list_item.subtitle);
            template.find('textarea[name=txt2]').summernote("code", list_item.content);
            template.find('.list_modal').modal('show');
        });

        $('#accordion.panel-group').sortable({ 
            handle: ".panel-heading .fa-arrows"
        });
        $('#accordion .list-group').sortable({ 
            handle: "span.fa-arrows"
        });
    });
</script>