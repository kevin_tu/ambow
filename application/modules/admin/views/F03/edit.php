
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item ">最新消息</li>
                    <li class="breadcrumb-item ">分類</li>
                    <li class="breadcrumb-item active">編輯</li>
                </ol>
            </h3>

        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form role="form">
                                <div class="form-group">
                                    <label>發佈</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="status" id="publish1" value="1" <?php echo @$row['status']==1?'checked':'' ?>>是
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="status" id="publish2" value="0" <?php echo @$row['status']!=1?'checked':'' ?>>否
                                    </label>
                                </div>
                                <!-- <div class="form-group">
                                    <label>使用外部連結</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="is_link" id="is_link1" value="1" <?php echo @$row['additional']['is_link']==1?'checked':'' ?>>是
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="is_link" id="is_link2" value="0" <?php echo @$row['additional']['is_link']!=1?'checked':'' ?>>否
                                    </label>
                                </div>
                                <div class="form-group outer_url">
                                    <label>外部連結</label>
                                    <input class="form-control" name="url" value="<?php echo @$row['additional']['url'] ?>">
                                    <p class="help-block"></p>
                                </div> -->
                                <div class="form-group ">
                                    <label>標題</label>
                                    <input class="form-control" name="title" value="<?php echo @$row['title'] ?>">
                                    <p class="help-block"></p>
                                </div>
                                <!-- <div class="form-group ">
                                    <label>層級</label>
                                    <select name="parent_id">
                                        <option value="0">主類別</option>
                                        <?php foreach (@$tags_all[0] as $tag): ?>
                                            <?php echo '<option value="' . $tag['id'] . '" ' . ($tag['id']==@$row['parent_id']?'selected':'') . '>' . $tag['title'] . '子類別</option>' ?>
                                        <?php endforeach ?>
                                    </select>
                                    <p class="help-block"></p>
                                </div> -->
                                <input type="hidden" name="parent_id" value="0">
                                <!-- <div class="form-group">
                                    <label>類別描述</label>
                                    <textarea class="form-control summernote" id="description" rows="3" name="description"><?php echo @$row['additional']['description'] ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label>圖片</label>
                                    <input type="hidden" class="img" id="img" data-size="1920x1280" data-title="" data-info="" value="<?php echo @$row['additional']['img'] ?>" >
                                    <p class="help-block"></p>
                                </div> -->
                                <a href="<?php echo site_url().'admin/'.$func.'/' ?>" class="btn btn-default">回列表</a>
                                <button type="submit" class="btn btn-default" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">儲存</button>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<script type="text/javascript">

    $(document).ready(function() {

        $('#img').myCoverUpload({
            img_url: '<?PHP echo site_url() . "upload/"?>',
            upload_url: '<?PHP echo site_url() . "admin/upload/image_upload"?>',
            //crop_url: '<?PHP echo site_url() . "admin/upload/crop"?>',
            thumbnail_type: '',
            thumbnail_size: [$('#img').attr('data-size')]
        });

        $('.summernote').summernote({
            height:300,
            toolbar:[
                ['insert',['link']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['codeview',['codeview']]
            ],
            onKeyup: function(e) {
                $(this).val($(this).code());
            },
        });

        $('form').on('submit',function(e){
            e.preventDefault();
            var q = {}
            q.id = "<?php echo @$row['id'] ?>";
            q.status = $('input[name=status]:checked').val();
            q.title = $('input[name=title]').val();
            q.parent_id = $('input[name=parent_id]').val();
            q.img = $('#img').val();
            q.description = $('#description').val();
            // q.is_link = $('input[name=is_link]:checked').val();
            // q.url = $('input[name="url"]').val();

            $('button[type=submit]').button('loading');
            $.post('',q,function(){
                $('button[type=submit]').button('reset');
                alert('儲存完成',  '',  'success');
            },'script');
        })

        $('input[name=is_link]').on('change',function(){
            if ($('input[name=is_link]:checked').val()==1) {
                $('.outer_url').show();
            }
            else{
                $('.outer_url').hide();
            }
        }).trigger('change');
    });
</script>