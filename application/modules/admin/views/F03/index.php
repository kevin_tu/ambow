
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item ">最新消息</li>
                        <?php if (!empty($parent_tag)): ?>
                        <li class="breadcrumb-item ">分類</li>
                        <li class="breadcrumb-item active"><?php echo $parent_tag['title'] ?></li>
                        <?php else: ?>
                        <li class="breadcrumb-item active">分類</li>
                        <?php endif ?>
                    </ol>
                </h3>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12 pull-right">
                <p>
                    <a href="<?php echo site_url() . 'admin/'.$func.'/edit' ?>" class="btn btn-default">新增一筆</a><br>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table width="100%" class="table table-responsive display select" cellspacing="0" id="datatable">
                            <thead>
                                <tr>
                                    <th>發佈</th>
                                    <th>標題</th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<script type="text/javascript">

    $(document).ready(function() {
        var config = {
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "pagingType": "simple_numbers",
            "language":{
                "processing":   "處理中...",
                "loadingRecords": "載入中...",
                "lengthMenu":   "顯示 _MENU_ 項結果",
                "zeroRecords":  "沒有符合的結果",
                "info":         "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
                "infoEmpty":    "顯示第 0 至 0 項結果，共 0 項",
                "infoFiltered": "(從 _MAX_ 項結果中過濾)",
                "infoPostFix":  "",
                "search":       "搜尋:",
                "paginate": {
                    "first":    "第一頁",
                    "previous": "上一頁",
                    "next":     "下一頁",
                    "last":     "最後一頁"
                },
                "aria": {
                    "sortAscending":  ": 升冪排列",
                    "sortDescending": ": 降冪排列"
                }
            },
            "columns": [
                {
                    'data': null,
                    'orderable': false,
                    'searchable':false,
                    'className': "center",
                    'render': function ( data, type, row ) {
                        return '<input type="checkbox" name="status" value="'+data.id+'" '+(data.status==1?'checked':'')+'>';
                    },
                    'width': '150px'
                },
                {"data": "title", 'orderable': false, 'searchable':true},
                {
                    'data': null,
                    'orderable': false,
                    'searchable':false,
                    'className': "center",
                    'render': function ( data, type, row ) {
                        // Combine the first and last names into a single table field
                        var btn = '<a class="" href="<?php echo site_url() . 'admin/' . $func ?>/edit/'+data.id+'">編輯</a>';
                        btn += ' <a class="" href="<?php echo site_url() . 'admin/' . $func ?>/?parent_id='+data.id+'">子類別</a>';
                        if (data.locked==null){
                            btn += ' <a class=" delete" data-id="'+data.id+'" href="<?php echo site_url() . 'admin/' . $func ?>/delete/'+data.id+'">刪除</a>';
                        }
                        return btn;
                    },
                    'width': '150px'
                    //defaultContent: '<a href="" class="editor_edit" data-id='++'>View</a>'
                }
            ],
            "ajax": {

                url: "<?php echo site_url() . 'admin/'.$func.'/list_json/' . $parent_id?>",
                type: 'POST'
            },
            "lengthMenu": [[20, 50, 100], [20, 50, 100]],
            "pageLength": parseInt('<?php echo $length ?>'),
            "displayStart": parseInt('<?php echo $start ?>')
        };
        var table = $('#datatable').DataTable(config);

        table.on('draw.dt',function(){ 
            //排序
            $('tbody').sortable({ 
                //handle: ".fa-arrows",
                helper:function(e,ui){
                    ui.children().each(function() {
                        $(this).width($(this).width());
                        $(this).height($(this).height());
                    });
                    return ui;
                },
                start: function (event, ui) {
                    
                },
                stop: function (event, ui) {
                    var tag_id = [];
                    $('input[name=status]').each(function(){
                        tag_id.push($(this).val());
                    })
                    var q = {};
                    q.tag_id = tag_id;
                    q.start = table.page.info().start;
                    $.post('<?php echo site_url()?>admin/<?php echo $func ?>/sort/',q);
                }
            });

            //刪除
            $('.delete').off().on('click',function(e){
                e.preventDefault();
                if (!confirm('確認刪除？')) return false;
                var tag_id = $(this).data('id');
                $.post('<?php echo site_url()?>admin/<?php echo $func ?>/delete/'+tag_id,'',function(){
                    table.draw();  
                })
            })

            //發佈
            $('input[name=status]').off().on('click',function(){
                var q = {};
                q.id = $(this).val();
                q.status = $(this).is(':checked')?1:0;
                console.log(q);
                $.post('<?php echo site_url()?>admin/<?php echo $func ?>/publish',q);
            })




        })
        .on('page.dt',function(){ //換頁
            gen_query_string(table.page.info().start,table.page.info().length,table.search());
        })
        .on('search.dt',function(){
            gen_query_string(table.page.info().start,table.page.info().length,table.search());
        })
        .on('length.dt',function(){
            gen_query_string(table.page.info().start,table.page.info().length,table.search());
        })

        <?php if (strlen(@$_GET['search'])): ?>
        table.search('<?php echo @$_GET["search"] ?>').draw(false);
        <?php endif; ?>
    });

    var gen_query_string = function(start,length,search){
        window.history.pushState(null, null, "?start="+start+"&length="+length+"&search="+search+"");
    }
</script>