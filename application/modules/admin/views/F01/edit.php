
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">网站参数管理</li>
                </ol>
            </h3>

        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form role="form">
                                <div class="form-group">
                                    <label>网站名称</label>
                                    <input class="form-control" name="meta_site_name" value="<?php echo @$row['meta_site_name'] ?>">
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group">
                                    <label>网站关键字</label>
                                    <textarea class="form-control " rows="3" name="meta_keyword"><?php echo @$row['meta_keyword'] ?></textarea>
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group">
                                    <label>网站描述</label>
                                    <textarea class="form-control " rows="3" name="meta_description"><?php echo @$row['meta_description'] ?></textarea>
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group">
                                    <label>社群分享照片(1200x630)</label>
                                    <input type="hidden" name="meta_ogimage" id="meta_ogimage" data-size="1200x630" data-title="" data-info="" value="<?php echo @$row['meta_ogimage'] ?>" >
                                    <p class="help-block"></p>
                                </div>
                                <button type="submit" class="btn btn-default" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">储存</button>
                            </form>
                        </div>
                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<script type="text/javascript">

    $(document).ready(function() {

        $('#meta_ogimage').myCoverUpload({
            img_url: '<?PHP echo site_url() . "upload/"?>',
            upload_url: '<?PHP echo site_url() . "admin/upload/crop_upload"?>',
            crop_url: '<?PHP echo site_url() . "admin/upload/crop"?>',
            thumbnail_type: 'crop',
            thumbnail_size: [$('#meta_ogimage').attr('data-size')]
        });

        $('.date').datepicker({ 
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        })

        $('form').on('submit',function(e){
            e.preventDefault();
            var q = {}
            q.meta_site_name = $('input[name=meta_site_name]').val();
            q.meta_keyword = $('textarea[name=meta_keyword]').val();
            q.meta_description = $('textarea[name=meta_description]').val();
            q.meta_ogimage = $('input[name=meta_ogimage]').val();

            $('button[type=submit]').button('loading');
            $.post('',q,function(){
                alert('储存完成')
                $('button[type=submit]').button('reset');
            },'script');
        })
    });
</script>