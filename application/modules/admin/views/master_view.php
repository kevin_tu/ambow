<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>安博教育</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo site_url() ?>assets/admin/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?PHP echo site_url();?>assets/admin/css/rowReorder.dataTables.min.css">

    <!-- MetisMenu CSS -->
    <link href="<?php echo site_url() ?>assets/admin/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo site_url() ?>assets/admin/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo site_url() ?>assets/admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?PHP echo site_url();?>assets/admin/css/jquery-ui.css">
    <link rel="stylesheet" href="<?PHP echo site_url();?>assets/admin/css/croppic.css">
    <link rel="stylesheet" href="<?PHP echo site_url();?>assets/admin/css/jquery.tagit.css">
    <link rel="stylesheet" href="<?PHP echo site_url();?>assets/admin/css/tagit-simple-grey.css">
    <link rel="stylesheet" href="<?PHP echo site_url();?>assets/admin/js/sweetalert2.min.css" />



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="<?php echo site_url() ?>assets/admin/vendor/jquery/jquery.min.js"></script>
    <script src="<?PHP echo site_url();?>assets/admin/js/jquery-ui.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo site_url() ?>assets/admin/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo site_url() ?>assets/admin/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo site_url() ?>assets/admin/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo site_url() ?>assets/admin/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo site_url() ?>assets/admin/vendor/datatables-responsive/dataTables.responsive.js"></script>
    <script src="<?PHP echo site_url();?>assets/admin/js/jquery.dataTables.rowReordering.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo site_url() ?>assets/admin/js/sb-admin-2.js"></script>
    <script src="<?PHP echo site_url();?>assets/admin/js/jquery.form.min.js"></script>
    <script src="<?PHP echo site_url();?>assets/admin/js/jquery.fileupload.js"></script>
    <script src="<?PHP echo site_url();?>assets/admin/js/croppic.min.js"></script>
    <script src="<?PHP echo site_url();?>assets/admin/js/jquery.mousewheel.min.js"></script>
    <script src="<?PHP echo site_url();?>assets/admin/js/tag-it.js"></script>
    <script type="text/javascript" src="<?PHP echo site_url();?>assets/admin/js/tagit-themeroller.js"></script>
    <script src="<?PHP echo site_url();?>assets/admin/js/sweetalert2.min.js"></script>

    <!-- include summernote css/js-->
    <link href="<?PHP echo site_url();?>assets/admin/css/summernote.css" rel="stylesheet">
    <script src="<?PHP echo site_url();?>assets/admin/js/summernote.js"></script>
    <link rel="stylesheet" type="text/css" href="<?PHP echo site_url();?>assets/admin/css/bootstrap-datetimepicker.min.css">
    <script type="text/javascript" src="<?PHP echo site_url();?>assets/admin/js/moment.min.js"></script>
    <script type="text/javascript" src="<?PHP echo site_url();?>assets/admin/js/bootstrap-datetimepicker.min.js"></script>

    <script src="<?PHP echo site_url();?>assets/admin/js/site.js"></script>
    <style type="text/css">
        body{
            font-family: "微軟正黑體";
        }
        .dataTables_paginate, .dataTables_filter{
            text-align: right;
        }
        .sorting:after, .sorting_asc:after, .sorting_desc:after {
            content:'' !important;
        }
        ul.tagit li.tagit-choice a.tagit-close{
            top:9px;
        }
    </style>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo site_url(); ?>" target="_blank">安博教育</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?php echo site_url() ?>admin/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <?php 
                            foreach($menu1 as $level_1){
                                if (count($level_1['children'])){
                                    $level_2_html = '';
                                    foreach($level_1['children'] as $level_2){
                                        if (count($level_2['children'])){
                                            $level_3_html = '';
                                            foreach($level_2['children'] as $level_3){
                                                if (count($level_3['children'])){
                                                    $level_4_html = '';
                                                    foreach($level_3['children'] as $level_4){
                                                        $level_4_html .= '
                                                            <li>
                                                                <a href="'.site_url() . 'admin/'.$level_4['sn'].'/"> ' . $level_4['name'] . '</a>
                                                            </li>';

                                                    }
                                                    $level_4_html = '<ul class="nav nav-4th-level">'.$level_4_html.'</ul>';
                                                    $level_3_html .= '
                                                        <li>
                                                            <a href="'.site_url() . 'admin/'.$level_3['sn'].'/"> ' . $level_3['name'] . '<span class="fa arrow"></span>' . '</a>
                                                            ' . $level_4_html . '
                                                        </li>';
                                                }
                                                else{
                                                    $level_3_html .= '
                                                        <li>
                                                            <a href="'.site_url() . 'admin/'.$level_3['sn'].'/"> ' . $level_3['name'] . '</a>
                                                        </li>';

                                                }
                                            }
                                            $level_3_html = '<ul class="nav nav-third-level">'.$level_3_html.'</ul>';
                                            $level_2_html .= '
                                                <li>
                                                    <a href="'.site_url() . 'admin/'.$level_2['sn'].'/"> ' . $level_2['name'] . '<span class="fa arrow"></span>' . '</a>
                                                    ' . $level_3_html . '
                                                </li>';
                                        }
                                        else{
                                            $level_2_html .= '
                                                <li>
                                                    <a href="'.site_url() . 'admin/'.$level_2['sn'].'/"> ' . $level_2['name'] . '</a>
                                                </li>';
                                        }

                                    }
                                    $level_2_html = '<ul class="nav nav-second-level">'.$level_2_html.'</ul>';
                                    echo '
                                        <li>
                                            <a href="'.site_url() . 'admin/'.$level_1['sn'].'/"> ' . $level_1['name'] . '<span class="fa arrow"></span>' . '</a>
                                            ' . $level_2_html . '
                                        </li>';
                                }
                                else{
                                    echo '
                                        <li>
                                            <a href="'.site_url() . 'admin/'.$level_1['sn'].'/"> ' . $level_1['name'] . '</a>
                                        </li>';
                                }
                            }
                        ?>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        
        <!-- Page Content -->
        <?php echo @$content_view; ?>
        <!-- /#page-wrapper -->
        <script type="text/javascript">
        </script>
    </div>
    <!-- /#wrapper -->
    <script type="text/javascript">
        function nl2br (str, is_xhtml) {   
            var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
            return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
        }
    </script>
</body>

</html>
