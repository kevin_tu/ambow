
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item ">首页轮播</li>
                    <li class="breadcrumb-item active">编辑</li>
                </ol>
            </h3>

        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <form role="form">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>前台显示</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="status" id="publish1" value="1" <?php echo @$row['status']==1?'checked':'' ?>>是
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="status" id="publish2" value="0" <?php echo @$row['status']!=1?'checked':'' ?>>否
                                    </label>
                                </div>
                                <div class="form-group ">
                                    <label>语系</label>
                                    <select name="tag[]">
                                        <!-- <option value="0">主類別</option> -->
                                        <?php foreach (@$level_1[0] as $tag): ?>
                                            <?php echo '<option value="' . $tag['id'] . '" ' . (in_array($tag['id'],$row['tags_selected'])?'selected':'') . ' >' . $tag['title'] . '</option>' ?>
                                        <?php endforeach ?>
                                    </select>
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group">
                                    <label>开启模式</label>
                                    <label class="radio-inline">
                                        <input type="radio" name="post_link_target" id="is_link1" value="1" <?php echo @$row['post_link_target']==1?'checked':'' ?>>本页开启
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="post_link_target" id="is_link2" value="0" <?php echo @$row['post_link_target']!=1?'checked':'' ?>>开启新分页
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>标题</label>
                                    <input class="form-control" name="post_title" value="<?php echo @$row['post_title'] ?>">
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group outer_url">
                                    <label>连结</label>
                                    <input class="form-control" name="post_link" value="<?php echo @$row['post_link'] ?>">
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group">
                                    <label>日期</label>
                                    <input class="form-control date" name="post_date" value="<?php echo date('Y-m-d',strtotime(@$row['createtime'])) ?>">
                                    <p class="help-block"></p>
                                </div>
                                <div class="form-group">
                                    <label>图片(1050x520)</label>
                                    <input type="hidden" name="post_cover" id="post_cover" data-size="1050x520" data-title="" data-info="" value="<?php echo @$row['post_cover'] ?>" >
                                    <p class="help-block"></p>
                                </div>
                            </div>
                        </div>
                        <!-- /.row (nested) -->
                        <div class="row">
                            <div class="col-lg-12">
                                <a href="<?php echo site_url().'admin/'.$func.'/' ?>" class="btn btn-default">回列表</a>
                                <button type="submit" class="btn btn-default" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">储存</button>
                                <?php if (@$row['id']!=''): ?>
                                <!-- <a href="<?php echo site_url().'admin/' . $func . '/preview/' . $row['id'] ?>" class="btn btn-default" target="_blank">預覽</a> -->
                                <?php endif ?>
                            </div>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </form>
</div>
<style type="text/css">
    /*.myGalleryUpload_item input[type=text]{display:none;}*/
</style>
<script type="text/javascript">

    $(document).ready(function() {

        $('#post_cover').myCoverUpload({
            img_url: '<?PHP echo site_url() . "upload/"?>',
            upload_url: '<?PHP echo site_url() . "admin/upload/crop_upload"?>',
            crop_url: '<?PHP echo site_url() . "admin/upload/crop"?>',
            thumbnail_type: 'crop',
            thumbnail_size: [$('#post_cover').attr('data-size')]
        });

        $('.date').datepicker({ 
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        })

        $('.summernote_simple').summernote({
            height:300,
            toolbar:[
                ['insert',['link']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['codeview',['codeview']]
            ],
            onKeyup: function(e) {
                $(this).val($(this).code());
            },
        });
        
        $('.summernote').summernote({
            prettifyHtml:true,
            toolbar: [
                // [groupName, [list of button]]
                // ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['fontsize', 'color']],
                // ['font', ['fontname']],
                ['para', ['paragraph']],
                ['insert', ['link','picture', 'doc', 'video']], // image and doc are customized buttons
                ['misc', ['codeview', 'fullscreen']],
            ],
            fontSizes: ['12', '15', '17'],
            callbacks: {
                onPaste: function (e) {
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                    e.preventDefault();
                    document.execCommand('insertText', false, bufferText);
                }
            }
        });
        $('.note-editable').css('font-size','15px');
        $('.note-current-fontsize').text('15');

        $('form').on('submit',function(e){
            e.preventDefault();

            var q = {}
            q.id = "<?php echo @$row['id'] ?>";
            q.status = $('input[name=status]:checked').val();
            q.post_title = $('input[name=post_title]').val();
            q.post_date = $('input[name=post_date]').val();
            q.post_cover = $('input[name=post_cover]').val();
            q.post_link = $('input[name=post_link]').val();
            q.post_link_target = $('input[name=post_link_target]:checked').val();
            
            q.tags = $('select[name="tag[]"] option:checked, input[name="tag[]"]:checked').map(function(){
                        return this.value;
                    }).get();

            // console.log(q); return false;
            $('button[type=submit]').button('loading');
            $.post('',q,function(){
                $('button[type=submit]').button('reset');
                alert('储存完成',  '',  'success');
            },'script')
        })
    });
</script>