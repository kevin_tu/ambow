<?php
$template_id = 'template_05_' . rand(1000,9999);
?>
<input type="hidden" name="template_id" value="05">
<div class="panel-heading">
    <h4 class="panel-title">
        <span class="fa fa-arrows"></span>
        <a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $template_id ?>">雙欄文字</a>
        <div class="pull-right"><a href="#" class="remove_template"><span class="fa fa-trash"></span></a></div>
    </h4>
</div>
<div id="<?php echo $template_id ?>" class="panel-collapse collapse in">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>文字</label>
                    <textarea class="form-control summer" id="txt_<?php echo $template_id ?>" rows="3" name="template_txt"><?php echo @$txt[0] ?></textarea>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label>文字</label>
                    <textarea class="form-control summer" id="txt_<?php echo $template_id ?>" rows="3" name="template_txt"><?php echo @$txt[1] ?></textarea>
                </div>
            </div>
        </div>
    </div>
</div>