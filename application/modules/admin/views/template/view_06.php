<?php
$template_id = 'template_06_' . rand(1000,9999);
?>
<input type="hidden" name="template_id" value="06">
<div class="panel-heading">
    <h4 class="panel-title">
        <span class="fa fa-arrows"></span>
        <a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $template_id ?>">清单</a>
        <div class="pull-right"><a href="#" class="remove_template"><span class="fa fa-trash"></span></a></div>
    </h4>
</div>
<div id="<?php echo $template_id ?>" class="panel-collapse collapse in">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>标题</label>
                    <textarea class="form-control " id="txt_<?php echo $template_id ?>" rows="3" name="template_txt"><?php echo @$txt[0] ?></textarea>
                </div>
                <div class="form-group">
                    <label>说明</label>
                    <textarea class="form-control " id="txt_<?php echo $template_id ?>" rows="3" name="template_txt"><?php echo @$txt[1] ?></textarea>
                </div>
            </div>
            <div class="col-lg-6">
                <ul class="list-group">
                    <?php if (!empty($list_data)): ?>
                    <?php foreach ($list_data as $value): ?>
                        <li class="list-group-item" id="list_75">
                            <span class="fa fa-arrows"></span> <a href="#" data-id="<?php echo $value['id'] ?>"><?php echo $value['title'] ?></a>
                        </li>
                    <?php endforeach ?>
                    <?php endif ?>
                </ul>
                <a href="#" class="add_list" data-toggle="modal" data-target="#exampleModal_<?php echo $template_id ?>">新增</a>
            </div>
        </div>
    </div>

    <div class="modal fade list_modal" id="exampleModal_<?php echo $template_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel_<?php echo $template_id ?>" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel_<?php echo $template_id ?>">编辑清单</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <input type="hidden" name="list_id" value="">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">标题</label>
                            <textarea class="form-control" name="txt1"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">副标题</label>
                            <textarea class="form-control" name="txt3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">文字</label>
                            <textarea class="form-control summer" name="txt2"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">关闭</button>
                    <button type="button" class="btn btn-danger delete" data-dismiss="modal">刪除</button>
                    <button type="button" class="btn btn-primary save" data-id="<?php echo $template_id ?>">储存</button>
                </div>
            </div>
        </div>
    </div>
</div>

