<?php
$template_id = 'template_04_' . rand(1000,9999);
?>
<input type="hidden" name="template_id" value="04">
<div class="panel-heading">
    <h4 class="panel-title">
        <span class="fa fa-arrows"></span>
        <a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $template_id ?>">左文字，右图</a>
        <div class="pull-right"><a href="#" class="remove_template"><span class="fa fa-trash"></span></a></div>
    </h4>
</div>
<div id="<?php echo $template_id ?>" class="panel-collapse collapse in">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>标题</label>
                    <input type="text" class="form-control" name="txt" placeholder="" value="<?php echo @$txt[0] ?>">
                </div>
                <div class="form-group">
                    <label>文字</label>
                    <textarea class="form-control summer" id="video_<?php echo $template_id ?>" rows="3" name="template_txt"><?php echo @$txt[1] ?></textarea>
                </div>
            </div>
            <div class="col-lg-6">
                <!-- div class="form-group">
                    <label class="radio-inline">
                        <input type="radio" name="options_1_<?php echo $template_id?>" id="optionsRadiosInline1" value="1" <?php echo @$option[0]!=2?'checked':'' ?>>圖片模式
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="options_1_<?php echo $template_id?>" id="optionsRadiosInline2" value="2" <?php echo @$option[0]==2?'checked':'' ?>>影片模式
                    </label>
                </div> -->
                <div class="form-group">
                    <label>图片(宽1200，高不限)</label>
                    <input type="hidden" class="img" id="img_<?php echo $template_id ?>" data-size="1200x0" data-title="" data-info="" value="<?php echo @$img[0] ?>" >
                    <p class="help-block"></p>
                    <input type="text" class="form-control" name="img_txt" placeholder="图片说明" value="<?php echo @$img_txt[0] ?>">
                </div>
                <<!-- div class="form-group">
                    <label>影片嵌入碼</label>
                    <textarea class="form-control" id="video_<?php echo $template_id ?>" rows="3" name="template_video"><?php echo @$video[0] ?></textarea>
                </div> -->
            </div>
        </div>
    </div>
</div>