<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends MY_Controller {

	public function index()	{
		
		$this->data['body_class'] = 'news';
		$this->data['meta']['page_title'] .= '-安博资讯';

		//置頂文章
		$this->db
			->select("F05.*")
			->join('tags', "tags.id=F05.tag_id")
			->where('tags.parent_id', 22) //語系
			->where('post_is_top', 1)
			->where('F05.status', 1)
			->where("createtime<'" . date('Y-m-d H:i:s') . "'")
			->order_by('sort desc, createtime desc');

		// echo $this->db->get_compiled_select('F05');exit;
		$query = $this->db->get('F05');

		$this->data['top_news'] = $query->row_array();
		// print_r($this->data['top_news']);

		//其他文章
		$query = $this->db
			->where('tag_id', 22) //語系
			->where('status', 1)
			->where('(F05.post_is_top is null or F05.post_is_top=0)')
			->where("createtime<'" . date('Y-m-d H:i:s') . "'")
			->get('F05');

		$record_total = $query->num_rows();

		$this->data['pagesize'] = 1;
		$this->data['page'] = isset($this->get['page']) ? $this->get['page'] : 1;
		$length = $this->data['pagesize'];
		$offset = ($this->data['page'] - 1) * $length;
		$query = $this->db
			->select("F05.*")
			->join('tags', "tags.id=F05.tag_id")
			->where('tags.parent_id', 22) //語系
			->where('(post_is_top is null or post_is_top=0)')
			->where('F05.status', 1)
			->where("createtime<'" . date('Y-m-d H:i:s') . "'")
			->order_by('sort desc, createtime desc')
			->get('F05', $length, $offset);

		$records = $query->result_array();



		$news = array(
			'record_total'=> $record_total,
			'records'=> $records,
		);
		$this->data['news'] = $news;
		$this->data['page_count'] = ceil($news['record_total'] / $this->data['pagesize']);

		// echo json_encode($this->data);exit;

		$this->data['content_view'] = $this->load->view('news_list_view', $this->data, true);
		$this->load->view('master_view', $this->data, FALSE);
	}

	public function category($tag_id=0)	{
		
		$this->data['body_class'] = 'news';
		$this->data['meta']['page_title'] .= '-安博资讯';
		
		$query = $this->db->get_where('tags', array('id'=> $tag_id));
		if ($query->num_rows()) {
			$this->data['category'] = $query->row_array();
		}
		else{
			show_404();
		}

		//置頂文章
		$this->db
			->select("F05.*")
			->join('tags', "tags.id=F05.tag_id")
			->where('tags.parent_id', 22) //語系
			->where('post_is_top', 1)
			->where('F05.status', 1)
			->where('F05.tag_id', $tag_id)
			->where("createtime<'" . date('Y-m-d H:i:s') . "'")
			->order_by('sort desc, createtime desc');

		// echo $this->db->get_compiled_select('F05');exit;
		$query = $this->db->get('F05');

		if (empty($tag_id)) {
			show_404();
		}

		$this->data['top_news'] = $query->row_array();
		// print_r($this->data['top_news']);

		//其他文章
		$query = $this->db
			->join('tags', "tags.id=F05.tag_id")
			->where('tags.parent_id', 22) //語系
			->where('F05.status', 1)
			->where('(F05.post_is_top is null or F05.post_is_top=0)')
			->where('F05.tag_id', $tag_id)
			->where("createtime<'" . date('Y-m-d H:i:s') . "'")
			->get('F05');

		$record_total = $query->num_rows();

		$this->data['pagesize'] = 1;
		$this->data['page'] = isset($this->get['page']) ? $this->get['page'] : 1;
		$length = $this->data['pagesize'];
		$offset = ($this->data['page'] - 1) * $length;
		$query = $this->db
			->select("F05.*")
			->join('tags', "tags.id=F05.tag_id")
			->where('tags.parent_id', 22) //語系
			->where('F05.post_is_top is null or F05.post_is_top=0')
			->where('F05.status', 1)
			->where('F05.tag_id', $tag_id)
			->where("createtime<'" . date('Y-m-d H:i:s') . "'")
			->order_by('sort desc, createtime desc')
			->get('F05', $length, $offset);

		$records = $query->result_array();



		$news = array(
			'record_total'=> $record_total,
			'records'=> $records,
		);
		$this->data['news'] = $news;
		$this->data['page_count'] = ceil($news['record_total'] / $this->data['pagesize']);

		// echo json_encode($this->data);exit;

		$this->data['content_view'] = $this->load->view('news_list_view', $this->data, true);
		$this->load->view('master_view', $this->data, FALSE);
	}


	public function detail($id=0){
		
		$this->data['body_class'] = 'news';
		$this->data['meta']['page_title'] .= '-安博资讯';

		$this->db
			->select("F05.*")
			->join('tags', "tags.id=F05.tag_id")
			->where('tags.parent_id', 22) //語系
			->where('F05.id', $id)
			->where('F05.status', 1);
		// echo $this->db->get_compiled_select('F05');exit;
		$query = $this->db->get('F05');
		if ($query->num_rows()==0) {
			show_404();
		}

		$news = $query->row_array();

		$template_data = json_decode($news['post_template'], 1);

		$template_html = array();
		if (!empty($template_data)) {
			foreach ($template_data as $value) {
				$template_id = $value['template_id'];
				if (isset($value['list_data'])) {
					$new_list_data = array();
					foreach ($value['list_data'] as $value2) {
						$new_list_data[] = $value2;
						$modal_data['list_data'][$value2['id']] = $value2;
					}
					$value['list_data'] = $new_list_data;
				}
				$template_html[] = $this->load->view('template/view_' . $template_id, $value, true);
			}
		}
		$this->data['news'] = $news;
		$this->data['template_html'] = $template_html;
		$this->data['content_view'] = $this->load->view('news_detail_view', $this->data, true);
		$this->load->view('master_view', $this->data, FALSE);
	}

}

/* End of file News.php */
/* Location: ./application/controllers/News.php */