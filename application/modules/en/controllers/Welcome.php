<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	public function index()	{
		$this->data['body_class'] = 'index';

		//banner
		$this->data['pagesize'] = 5;
		$this->data['page'] = isset($this->get['page']) ? $this->get['page'] : 1;
		$length = $this->data['pagesize'];
		$offset = ($this->data['page'] - 1) * $length;
		$query = $this->db
			->select("F06.*")
			->where('tag_id', 22)
			->where('F06.status', 1)
			->where("createtime<'" . date('Y-m-d H:i:s') . "'")
			->order_by('sort desc, createtime desc')
			->get('F06', $length, $offset);

		$banners = $query->result_array();
		$this->data['banners'] = $banners;


		//置頂文章
		$this->db
			->select("F05.*")
			->join('tags', "tags.id=F05.tag_id")
			->where('tags.parent_id', 22) //語系
			->where('post_is_top', 1)
			->where('F05.status', 1)
			->where("createtime<'" . date('Y-m-d H:i:s') . "'")
			->order_by('createtime desc');

		// echo $this->db->get_compiled_select('F05');exit;
		$query = $this->db->get('F05');

		$this->data['top_news'] = $query->row_array();
		// print_r($this->data['top_news']);

		//其他文章
		$query = $this->db
			->where('tag_id', 22) //語系
			->where('status', 1)
			->where('(post_is_top is null or post_is_top=0)')
			->where("createtime<'" . date('Y-m-d H:i:s') . "'")
			->get('F05');

		$record_total = $query->num_rows();

		$this->data['pagesize'] = 5;
		$this->data['page'] = isset($this->get['page']) ? $this->get['page'] : 1;
		$length = $this->data['pagesize'];
		$offset = ($this->data['page'] - 1) * $length;
		$query = $this->db
			->select("F05.*")
			->join('tags', "tags.id=F05.tag_id")
			->where('tags.parent_id', 22) //語系
			->where('(post_is_top is null or post_is_top=0)')
			->where('F05.status', 1)
			->where("createtime<'" . date('Y-m-d H:i:s') . "'")
			->order_by('createtime desc')
			->get('F05', $length, $offset);

		$records = $query->result_array();



		$news = array(
			'record_total'=> $record_total,
			'records'=> $records,
		);
		$this->data['news'] = $news;


		$this->data['content_view'] = $this->load->view('index_view', $this->data, true);
		$this->load->view('master_view', $this->data, FALSE);
	}

}

/* End of file Welcome.php */
/* Location: ./application/controllers/Welcome.php */