<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends MY_Controller {


	public function detail($id='') {

		$this->data['body_class'] = 'news';


		if ($id=='') {
			show_404();
		}

		$query = $this->db->where('id', $id)->where('status', 1)->get('tags');
		if ($query->num_rows()==0) {
			show_404();
		}

		$row = $query->row_array();
		$additional = json_decode($row['additional'], 1);
		$template_data = $additional['template_data'];

		$template_html = array();
		if (!empty($template_data)) {
			foreach ($template_data as $value) {
				$template_id = $value['template_id'];
				if (isset($value['list_data'])) {
					$new_list_data = array();
					foreach ($value['list_data'] as $value2) {
						$new_list_data[] = $value2;
						$modal_data['list_data'][$value2['id']] = $value2;
					}
					$value['list_data'] = $new_list_data;
				}
				$template_html[] = $this->load->view('template/view_' . $template_id, $value, true);
			}
		}

		$this->data['meta']['page_title'] .= '-' . $row['title'];

		$this->data['page_data'] = $row;
		$this->data['template_html'] = join('', $template_html);

		$this->data['content_view'] = $this->load->view('page_view', $this->data, true);
		$this->load->view('master_view', $this->data, FALSE);
	}

}

/* End of file Page.php */
/* Location: ./application/controllers/Page.php */