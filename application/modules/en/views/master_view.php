<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <!-- Global site tag (gtag.js) - Google Analytics-->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-136170536-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      
      gtag('config', 'UA-136170536-1');
      
    </script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="keywords" content="<?php echo @$meta['meta_keyword'] ?>">
    <meta name="description" content="<?php echo @$meta['meta_description'] ?>">
    <meta property="og:title" content="<?php echo @$meta['meta_site_name'] ?>">
    <meta property="og:site_name" content="<?php echo @$meta['meta_site_name'] ?>">
    <meta property="og:url" content="<?php echo $page_url ?>">
    <meta property="og:image" content="<?php echo @$meta['meta_ogimage'] ?>">
    <meta property="og:description" content="<?php echo @$meta['meta_description'] ?>">
    <meta property="og:type" content="website">
    <meta property="fb:admin" content="">
    <title><?php echo @$meta['page_title'] ?>
    </title>
    <link rel="shortcut icon" type="png" href="<?php echo site_url() ?>assets/images/favicon.png">
    <link rel="stylesheet" media="screen" type="text/css" href="<?php echo site_url() ?>assets/css/style.css"><!--[if lte IE 11]>
    <link rel="stylesheet" media="screen" type="text/css" href="assets/css/ie.css"><![endif]-->
  </head>
  <body class="<?php echo @$body_class ?> en">
    <div class="header">
      <div class="logoIndex"><a href="<?php echo site_url('en') ?>"><img src="<?php echo site_url() ?>assets/images/img-logo_index_en@2x.png"></a></div>
      <div class="language"><a href="<?php echo site_url() ?>">CH</a><a class="is-current" href="<?php echo site_url('en') ?>">EN</a></div>
    </div>
    <div class="menuToggle"><span></span><span></span></div>
    <div class="nav">
      <ol>
        <?php foreach ($menu as $key=>$value): ?>
          <?php if ($value['id']==6): //新聞 ?>
          <li class="<?php echo !empty($value['children'])?'has-sub':'' ?>">
            <p><a href="<?php echo site_url('news') ?>"><?php echo $value['title'] ?></a></p>
            <?php if (!empty($value['children'])): ?>
            <ul>
              <li><a href="<?php echo site_url('news') ?>">所有</a></li>
              <?php foreach ($value['children'] as $value2): ?>
              <li><a href="<?php echo site_url('news/category/' . $value2['id']) ?>"><?php echo $value2['title'] ?></a></li>
              <?php endforeach ?>
            </ul>
            <?php endif ?>
          </li>
          <?php elseif ($value['id']==57): //新聞 ?>
          <li class="<?php echo !empty($value['children'])?'has-sub':'' ?>">
            <p><a href="<?php echo site_url('en/news') ?>"><?php echo $value['title'] ?></a></p>
            <?php if (!empty($value['children'])): ?>
            <ul>
              <li><a href="<?php echo site_url('en/news') ?>">All</a></li>
              <?php foreach ($value['children'] as $value2): ?>
              <li><a href="<?php echo site_url('en/news/category/' . $value2['id']) ?>"><?php echo $value2['title'] ?></a></li>
              <?php endforeach ?>
            </ul>
            <?php endif ?>
          </li>
          <?php else: //一般頁面 ?>
          <li class="<?php echo !empty($value['children'])?'has-sub':'' ?>">
            <?php if (!empty($value['children'])): ?>
            <p><?php echo $value['title'] ?></p>
            <ul>
              <?php foreach ($value['children'] as $value2): ?>
              <li><a href="<?php echo site_url('en/page/' . $value2['id']) ?>"><?php echo $value2['title'] ?></a></li>
              <?php endforeach ?>
            </ul>
            <?php else: ?>
            <a href="<?php echo site_url('en/page/' . $value['id']) ?>"><p><?php echo $value['title'] ?></p></a>
            <?php endif ?>
          </li>
          <?php endif ?>
        <?php endforeach ?>
      </ol>
      <div class="language"><a href="<?php echo site_url() ?>">CH</a><a class="is-current" href="<?php echo site_url('en') ?>">EN</a></div>
    </div>
    <div class="grid">
      <div class="col-left"></div>
      <div class="col-center"><span></span><span></span></div>
      <div class="col-right"></div>
    </div>

    <?php echo $content_view; ?>

    <div class="footer">
      <div class="js-goTop"></div>
      <div class="container">
        <div class="footer-content">
          <div class="footer-info">
            <div class="title">Ambow Education Group</div>
            <div class="text">
              <p>Add：12th floor,No.1 Financial Street Chang’An Center,Shijingshan District,Beijing,China(100043)</p>
              <p>Tel: 010-62068000</p>
              <p>Fax: 010-62068100</p>
              <p>Email | <a href="mailto:marketing@ambow.com" target="_blank">marketing@ambow.com</a></p>
            </div>
          </div>
          <div class="footer-nav">
            <?php foreach ($menu as $key=>$value): ?>
              <?php if (in_array($value['id'], array(19, 55, 56, 58))): ?>
              <div class="list">
                <?php if (!empty($value['children'])): ?>
                <div class="title"><?php echo $value['title'] ?></div>
                <ol>
                  <?php foreach ($value['children'] as $value2): ?>
                  <li><a href="<?php echo site_url('en/page/' . $value2['id']) ?>"><?php echo $value2['title'] ?></a></li>
                  <?php endforeach ?>
                </ol>
                <?php else: ?>
                <div class="title"><a href="<?php echo site_url('en/page/' . $value['id']) ?>"><?php echo $value['title'] ?></a></div>
                <?php endif ?>
              </div>
              <?php endif ?>
            <?php endforeach ?>
            <div class="list">
              <div class="link"><a href="<?php echo site_url('en/page/' . $value['id']) ?>">Press Releases</a></div>
              <div class="link"><a href="#">Social Responsibility</a></div>
              <div class="link"><a href="#">Contact Us</a></div>
            </div>
          </div>
        </div>
        <div class="footer-bottom"> 
          <div class="socialMedia">
            <ol>
              <!-- <li><a target="_blank" href="https://www.wechat.com/cgi-bin/readtemplate?t=market_redirect"><img src="<?php echo site_url() ?>assets/images/icon-wechat@2x.png"></a></li> -->
              <li class="js-wechat"><img src="<?php echo site_url() ?>assets/images/icon-wechat@2x.png">
                <div class="qrcode"><img src="<?php echo site_url() ?>assets/images/img-wechat-qrcode.png"></div>
              </li>
              <li><a target="_blank" href="https://baijiahao.baidu.com/u?app_id=1605589294258622&fr=bjharticle"><img src="<?php echo site_url() ?>assets/images/icon-baidu@2x.png"></a></li>
              <li><a target="_blank" href="http://blog.sina.com.cn/u/1659650625"><img src="<?php echo site_url() ?>assets/images/icon-sina@2x.png"></a></li>
              <li><a target="_blank" href="https://weibo.com/p/1006061659650625/home?from=page_100606&mod=TAB&is_all=1#place"><img src="<?php echo site_url() ?>assets/images/icon-weibo@2x.png"></a></li>
              <li><a target="_blank" href="https://kuaibao.qq.com/s/MEDIANEWSLIST?chlid=10571085"><img src="<?php echo site_url() ?>assets/images/icon-qq@2x.png"></a></li>
              <li><a target="_blank" href="https://www.toutiao.com/c/user/6779096761/#mid=6788953917"><img src="<?php echo site_url() ?>assets/images/icon-toutiao@2x.png"></a></li>
              <li><a target="_blank" href="http://mp.sohu.com/profile?xpt=MjA3MDY1NjQ5OUBxcS5jb20=&_f=index_pagemp_1"><img src="<?php echo site_url() ?>assets/images/icon-ss@2x.png"></a></li>
            </ol>
          </div>
          <div class="copyright">Copyright©2019 安博教育集团 版权所有 | 京ICP备05050</div>
        </div>
      </div>
    </div>
    <div class="grid">
      <div class="col-left"></div>
      <div class="col-center"><span></span><span></span></div>
      <div class="col-right"></div>
    </div>
    <script src="<?php echo site_url() ?>assets/js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo site_url() ?>assets/js/jquery.bxslider.min.js"></script>
    <script src="<?php echo site_url() ?>assets/js/app.js"></script>
  </body>
</html>