

      <!-- 加入安博-->
      <div class="container">
        <div class="join">
          <div class="item title">
            <h3><?php echo nl2br(@$txt[0]) ?></h3>
            <p><?php echo nl2br(@$txt[1]) ?></p>
          </div>
          <div class="item lists">
            <?php if (!empty($list_data)): ?>
            <?php foreach ($list_data as $value): ?>
            <div class="item-list">
              <h3><?php echo @$value['title'] ?> </h3>
              <h5><?php echo @$value['subtitle'] ?></h5>
              <div class="item-content">
                <?php echo @$value['content'] ?>
              </div>
            </div>
            <?php endforeach ?>
            <?php endif ?>
          </div>
        </div>
      </div>
