<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends MX_Controller {

	protected $site_key = 'ambowsdoajdojdiojasoidjaosdjasodjas';
	protected $shipping = 100;
	protected $no_shipping = 400;
	
	function __construct() {
		parent::__construct();
		session_start();

		$this->load->database();
		$this->load->helper('url');


		$this->post = $this->input->post(null,true);
		$this->get = $this->input->get(null,true);
		$this->data = array();
        $this->data['page_url'] = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

		date_default_timezone_set('Asia/Taipei');
		set_time_limit(0);

		ini_set('error_reporting', E_ALL | E_STRICT);
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		ini_set('memory_limit', '256M');

		$this->db->query("SET SESSION SQL_BIG_SELECTS=1");

		


		


		$sql = "select 
			(select meta_value from meta where meta_key='meta_site_name') as meta_site_name,
			(select meta_value from meta where meta_key='meta_keyword') as meta_keyword,
			(select meta_value from meta where meta_key='meta_description') as meta_description,
			concat('".site_url() . 'upload/'."',(select meta_value from meta where meta_key='meta_ogimage')) as meta_ogimage";

		$query = $this->db->query($sql);
		$this->data['meta'] = $query->row_array();
		$this->data['meta']['meta_ogimage_w'] = 1200;
		$this->data['meta']['meta_ogimage_h'] = 630;
		$this->data['meta']['page_title'] = $this->data['meta']['meta_site_name'];

		if ($this->uri->segment(1) == 'en') {
			$this->data['menu'] = $this->get_menu('F02', 2);
		}
		else{
			$this->data['menu'] = $this->get_menu('F02', 1);
		}

		// echo json_encode($this->data['menu']);exit;

	}

	//選單
	private function get_menu($type='F02', $parent=1){

		$tags = $this->db
			->where('tag_type', $type)
			->where('parent_id', $parent)
			->where('status=1')
			->order_by('sort desc, id desc')
			->get('tags')->result_array();
		$menu = array();
		foreach ($tags as $key => $value) {
			if ($value['id']=='6') {
				$query = $this->db
					->where('tag_type', 'F04')
					->where('parent_id', '21')
					->where('status=1')
					->order_by('sort desc, id desc')
					->get('tags');
			}
			elseif ($value['id']=='57') {
				$query = $this->db
					->where('tag_type', 'F04')
					->where('parent_id', '22')
					->where('status=1')
					->order_by('sort desc, id desc')
					->get('tags');
			}
			else{
				$query = $this->db
					->where('tag_type', $type)
					->where('parent_id', $value['id'])
					->where('status=1')
					->order_by('sort desc, id desc')
					->get('tags');
			}
			if ($query->num_rows()>0) {
				$value['children'] = $query->result_array();
			}
			$menu[] = $value;

		}
		// echo json_encode($menu);exit;
		return $menu;
	}

	public function check_auth($func){
		if (is_array(@$_SESSION['admin_user']['auth'])){

			if (!in_array($func,@$_SESSION['admin_user']['auth'])) {
				unset($_SESSION['admin_user']);
				echo '沒有權限';
				echo '<script>location="'.site_url().'admin/";</script>';
				exit;
			}
		}
		else{
			unset($_SESSION['admin_user']);
			echo '<script>location="'.site_url().'admin/";</script>';
		}
	}

}

class MY_Controller_Admin extends MY_Controller {


	function __construct() {
		parent::__construct();


		if (!isset($_SESSION['admin_user'])){
			if ($this->input->is_ajax_request()) {
				echo 'alert("請重新登入"); location="'.site_url().'admin/login";';
				header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
				exit;
			}
			else{
				header('location:'.site_url().'admin/login');
				exit;

			}
		}

		$this->data['menu1'] = $this->get_menu();
		$this->data['menu_all'] = $this->get_menu_all();
	}

	public function parse_template($template_data){
		$template_html = '';
		$template_data = json_decode($template_data,1);
		if (is_array($template_data)){
			foreach($template_data as $settings){
				$template = $this->load->view('admin/template/view_' . $settings['template_id'], $settings, true);
				$template_html .= '<div class="panel panel-default">' . $template . '</div>';
			}
		}
		return $template_html;
	}

	public function get_menu(){
		$level_1 = $this->db
			->order_by('sort')
			->where('level',1)
			->where('status',1)->where_in('sn',@$_SESSION['admin_user']['auth'])
			->get('admin_func')->result_array();

		$menu = array();
		foreach($level_1 as $row){
			/*上線後改文字*/
			if ($row['sn']=='F02') {
				$row['name'] = '一般页面';
			}

			$row['children'] = array();
			$level_2 = $this->db->where(array('level'=>2,'parent'=>$row['sn'],'status'=>1))->where_in('sn',@$_SESSION['admin_user']['auth'])->order_by('sort')->get('admin_func')->result_array();
			foreach($level_2 as $row2){
				$row2['children'] = array();
				$level_3 = $this->db->where(array('level'=>3,'parent'=>$row2['sn'],'status'=>1))->where_in('sn',@$_SESSION['admin_user']['auth'])->order_by('sort')->get('admin_func')->result_array();
				foreach($level_3 as $row3){
					$row3['children'] = array();
					$level_4 = $this->db->where(array('level'=>4,'parent'=>$row3['sn'],'status'=>1))->where_in('sn',@$_SESSION['admin_user']['auth'])->order_by('sort')->get('admin_func')->result_array();
					foreach($level_4 as $row4){
						$row3['children'][] = $row4;
					}
					$row2['children'][] = $row3;
				}
				$row['children'][] = $row2;
			}
			$menu[] = $row;
		}

		return $menu;
	}

	public function get_menu_all(){
		$level_1 = $this->db
			->order_by('sort')
			->where('level',1)
			->where('status',1)
			->get('admin_func')->result_array();

		$menu = array();
		foreach($level_1 as $row){
			$row['children'] = array();
			$level_2 = $this->db->where(array('level'=>2,'parent'=>$row['sn'],'status'=>1))->order_by('sort')->get('admin_func')->result_array();
			foreach($level_2 as $row2){
				$row2['children'] = array();
				$level_3 = $this->db->where(array('level'=>3,'parent'=>$row2['sn'],'status'=>1))->order_by('sort')->get('admin_func')->result_array();
				foreach($level_3 as $row3){
					$row3['children'] = array();
					$level_4 = $this->db->where(array('level'=>4,'parent'=>$row3['sn'],'status'=>1))->order_by('sort')->get('admin_func')->result_array();
					foreach($level_4 as $row4){
						$row3['children'][] = $row4;
					}
					$row2['children'][] = $row3;
				}
				$row['children'][] = $row2;
			}
			$menu[] = $row;
		}

		return $menu;

	}

}
/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */