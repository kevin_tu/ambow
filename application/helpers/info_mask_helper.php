<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
/* 
  filename without extension 
  ex: file_core_name('toto.jpg') -> 'toto'
*/
if ( ! function_exists('info_mask'))
{
	function info_mask($str, $type)
	{

		$new_str = '';
		switch ($type) {
			case 'name':
				for ($i = 0; $i < mb_strlen($str); $i++) {
					if (in_array($i, array(1))) {
						$new_str .= '*';
					}
					else {
						$new_str .= mb_substr($str, $i, 1);
					}
				}
				break;
			case 'phone':
				for ($i = 0; $i < mb_strlen($str); $i++) {
					if (in_array($i, array(4, 5, 6))) {
						$new_str .= '*';
					}
					else {
						$new_str .= mb_substr($str, $i, 1);
					}
				}
				break;
			case 'email':
				for ($i = 0; $i < mb_strlen($str); $i++) {
					if (in_array($i, array(4,5,6,7,8))) {
						$new_str .= '*';
					}
					else {
						$new_str .= mb_substr($str, $i, 1);
					}
				}
				break;
			case 'address':
				for ($i = 0; $i < mb_strlen($str); $i++) {
					if (in_array($i, array(4,5,6,7,8))) {
						$new_str .= '*';
					}
					else {
						$new_str .= mb_substr($str, $i, 1);
					}
				}
				break;
			
			default:
				$new_str = $str;
				// code...
				break;
		}

		return $new_str;
	}
}
 