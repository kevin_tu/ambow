

      <!-- 右图左文范本-->
      <div class="news-article news-article04">
        <div class="article-photo">
            <?php if (!empty($img[0])): ?>
            <img src="<?php echo site_url('upload/' . @$img[0]) ?>">
            <?php endif ?>
        </div>
        <div class="article-content">
          <div class="article-title"><?php echo @$txt[0] ?></div>
          <div class="article-description">
            <?php echo @$txt[1] ?>
          </div>
        </div>
      </div>