
    <div id="main-wrapper">
      <h1 class="sideTitle left">AMBOW</h1>
      <h1 class="sideTitle right">EDUCATION</h1>
      <div class="news-category">
        <div class="ch">安博资讯</div>
        <div class="en">Ambow News</div>
        <div class="categoryName"><?php echo @$category['title'] ?></div>
      </div>
      <div class="container">
        <?php if (!empty($top_news)): ?>
        <!-- 列表置顶-->
        <div class="news-list-top">
          <div class="news-list-item">
            <div class="item-cover">
              <?php if (!empty($top_news['post_cover'])): ?>
              <img src="<?php echo site_url('upload/' . $top_news['post_cover']) ?>">
              <?php endif ?>
            </div>
            <div class="item-info">
              <div class="item-decoration">Featuring</div>
              <div class="item-content"><a href="<?php echo site_url('news/detail/' . $top_news['id']) ?>">
                  <div class="item-description"><?php echo $top_news['title'] ?></div></a></div>
              <div class="item-bottom">
                <div class="category"><img src="<?php echo site_url() ?>assets/images/icon-tag.png"/><?php echo $top_news['tag_title'] ?></div>
                <div class="date"><img src="<?php echo site_url() ?>assets/images/icon-clock.png"/><?php echo date('j M, Y', strtotime($top_news['createtime'])) ?></div>
              </div>
            </div>
          </div>
        </div>
        <?php endif ?>
        <!-- 新闻列表-->
        <div class="news-list flex" style="margin-top:70px;">
          <?php foreach ($news['records'] as $value): ?>
          <div class="news-list-item col-2">
            <div class="item-content"><a href="<?php echo site_url('news/detail/' . $value['id']) ?>">
                <div class="item-description"><?php echo $value['title'] ?></div></a></div>
            <div class="item-bottom">
              <div class="category"><img src="<?php echo site_url() ?>assets/images/icon-tag.png"/><?php echo $value['tag_title'] ?></div>
              <div class="date"><img src="<?php echo site_url() ?>assets/images/icon-clock@2x.png"/><?php echo date('j M, Y', strtotime($value['createtime'])) ?></div>
            </div>
          </div>
          <?php endforeach ?>
        </div>
        <div class="pagenation">
          <ol>
            <?php for ($i = 1; $i <= $page_count; $i++){
                echo '<li class="' . ($i==$page ? 'is-active' : '') . '"><a href="'.site_url('news/?page=' . $i).'">' . $i . '</a></li>';
            } ?>
          </ol>
        </div>
      </div>
    </div>