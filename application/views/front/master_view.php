<!DOCTYPE html>
<html lang="zh-TW">
  <head>
    <!-- Global site tag (gtag.js) - Google Analytics-->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-136170536-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      
      gtag('config', 'UA-136170536-1');
      
    </script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="keywords" content="<?php echo @$meta['meta_keyword'] ?>">
    <meta name="description" content="<?php echo @$meta['meta_description'] ?>">
    <meta property="og:title" content="<?php echo @$meta['meta_site_name'] ?>">
    <meta property="og:site_name" content="<?php echo @$meta['meta_site_name'] ?>">
    <meta property="og:url" content="<?php echo $page_url ?>">
    <meta property="og:image" content="<?php echo !empty($meta['meta_ogimage']) ?>">
    <meta property="og:description" content="<?php echo @$meta['meta_description'] ?>">
    <meta property="og:type" content="website">
    <meta property="fb:admin" content="">
    <title><?php echo @$meta['page_title'] ?>
    </title>
    <link rel="shortcut icon" type="png" href="<?php echo site_url() ?>assets/images/favicon.png">
    <link rel="stylesheet" media="screen" type="text/css" href="<?php echo site_url() ?>assets/css/style.css"><!--[if lt IE 10]>
    <link rel="stylesheet" media="screen" type="text/css" href="assets/css/ie.css"><![endif]-->
  </head>
  <body class="<?php echo @$body_class ?>">
    <div class="header">
      <div class="logoIndex"><a href="<?php echo site_url() ?>"><img src="<?php echo site_url() ?>assets/images/img-logo_index@2x.png"></a></div>
      <div class="language"><a class="is-current" href="<?php echo site_url() ?>">CH</a><a href="<?php echo site_url('en') ?>">EN</a></div>
    </div>
    <div class="menuToggle"><span></span><span></span></div>
    <div class="nav">
      <ol>
        <?php foreach ($menu as $key=>$value): ?>
          <?php if ($value['id']==6): //新聞 ?>
          <li class="<?php echo !empty($value['children'])?'has-sub':'' ?>">
            <p><?php echo $value['title'] ?></p>
            <?php if (!empty($value['children'])): ?>
            <ul>
              <li><a href="<?php echo site_url('news') ?>">所有</a></li>
              <?php foreach ($value['children'] as $value2): ?>
              <li><a href="<?php echo site_url('news/category/' . $value2['id']) ?>"><?php echo $value2['title'] ?></a></li>
              <?php endforeach ?>
            </ul>
            <?php endif ?>
          </li>
          <?php else: //一般頁面 ?>
          <li class="<?php echo !empty($value['children'])?'has-sub':'' ?>">
            <?php if (!empty($value['children'])): ?>
            <p><?php echo $value['title'] ?></p>
            <ul>
              <?php foreach ($value['children'] as $value2): ?>
              <li><a href="<?php echo site_url('page/' . $value2['id']) ?>"><?php echo $value2['title'] ?></a></li>
              <?php endforeach ?>
            </ul>
            <?php else: ?>
            <a href="<?php echo site_url('page/' . $value['id']) ?>"><p><?php echo $value['title'] ?></p></a>
            <?php endif ?>
          </li>
          <?php endif ?>
        <?php endforeach ?>
      </ol>
      <div class="language"><a class="is-current" href="<?php echo site_url() ?>">CH</a><a href="<?php echo site_url('en') ?>">EN</a></div>
    </div>
    <div class="grid">
      <div class="col-left"></div>
      <div class="col-center"><span></span><span></span></div>
      <div class="col-right"></div>
    </div>

    <?php echo $content_view; ?>

    <div class="footer">
      <div class="js-goTop"></div>
      <div class="container">
        <div class="footer-content">
          <div class="footer-info">
            <div class="title">安博教育集团</div>
            <div class="text">
              <p>地址 | 北京石景山区金融街长安中心1号楼12层</p>
              <p>邮编 | 100043</p>
              <p>总机 | 010-62068000（工作时间：早9:00—18:00）</p>
              <p>传真 | 010-62068100</p>
              <p>Email | <a href="mailto:marketing@ambow.com" target="_blank">marketing@ambow.com</a></p>
            </div>
          </div>
          <div class="footer-nav">
            <?php foreach ($menu as $key=>$value): ?>
              <?php if (in_array($value['id'], array(3, 4, 5, 7))): ?>
              <div class="list">
                <?php if (!empty($value['children'])): ?>
                <div class="title"><?php echo $value['title'] ?></div>
                <ol>
                  <?php foreach ($value['children'] as $value2): ?>
                  <li><a href="<?php echo site_url('page/' . $value2['id']) ?>"><?php echo $value2['title'] ?></a></li>
                  <?php endforeach ?>
                </ol>
                <?php else: ?>
                <div class="title"><a href="<?php echo site_url('page/' . $value['id']) ?>"><?php echo $value['title'] ?></a></div>
                <?php endif ?>
              </div>
              <?php endif ?>
            <?php endforeach ?>
            <div class="list">
              <div class="link"><a href="#">安博资讯</a></div>
              <div class="link"><a href="#">社会责任</a></div>
              <div class="link"><a href="#">联系我们</a></div>
            </div>
          </div>
        </div>
        <div class="footer-bottom"> 
          <div class="socialMedia">
            <ol>
              <li><a href="#"><img src="<?php echo site_url() ?>assets/images/icon-wechat@2x.png"></a></li>
              <li><a href="#"><img src="<?php echo site_url() ?>assets/images/icon-baidu@2x.png"></a></li>
              <li><a href="#"><img src="<?php echo site_url() ?>assets/images/icon-sina@2x.png"></a></li>
              <li><a href="#"><img src="<?php echo site_url() ?>assets/images/icon-weibo@2x.png"></a></li>
              <li><a href="#"><img src="<?php echo site_url() ?>assets/images/icon-qq@2x.png"></a></li>
              <li><a href="#"><img src="<?php echo site_url() ?>assets/images/icon-toutiao@2x.png"></a></li>
              <li><a href="#"><img src="<?php echo site_url() ?>assets/images/icon-ss@2x.png"></a></li>
            </ol>
          </div>
          <div class="copyright">Copyright©2019 安博教育集团 版权所有 | 京ICP备05050</div>
        </div>
      </div>
    </div>
    <div class="grid">
      <div class="col-left"></div>
      <div class="col-center"><span></span><span></span></div>
      <div class="col-right"></div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.js"></script>
    <script src="<?php echo site_url() ?>assets/js/app.js"></script>
    <style type="text/css">

    <!--右側英文字-->
    @media only screen and (max-width: 320px) {
      .businesses h1.sideTitle.right,.news h1.sideTitle.right {
          transform-origin: right 0;
          top: 47%;
          right: -10px
      }
    }

      
    </style>
    <script type="text/javascript">
      

      var url = window.location.href;
      var separators = ['\\\?'];
      var url = url.split(new RegExp(separators.join('|'), 'g'));
      // var element = $('ul.nav a').filter(function() {
      //     return this.href == url;
      // }).addClass('active').parent().parent().addClass('in').parent();
      var element = $('div.nav a').filter(function() {
          return this.href == url[0];
      }).parent().addClass('is-current');

      // element.parents('li').each(function(){
      //     $(this).addClass('is-active');
      // })
    </script>
  </body>
</html>