
    <div id="main-wrapper">
      <!-- 首页 Slider-->
      <div class="slider">
        <div class="floatWord">
          <div class="ambow">
            <p>AMBOW</p>
            <p>EDUCATION</p>
          </div><span>Featuring</span>
        </div>
        <ul class="bxslider">
          <?php foreach ($banners as $value): ?>
          <li>
          <?php if (!empty($value['post_cover'])): ?>
            <a href="<?php echo $value['post_link'] ?>">
              <div class="slider-cover">
                <img src="<?php echo site_url('upload/' . $value['post_cover']) ?>">
              </div>
              <div class="slider-content" style="min-height: 224px">
                <div class="title"><?php echo $value['title'] ?></div>
              </div>
            </a>
          <?php endif ?>
          </li>
          <?php endforeach ?>
        </ul>
      </div>
      <div class="container">
        <div class="index-intro">
          <div class="item col-1"><a href="<?php echo site_url('page/40') ?>">
              <div class="item-icon"><img src="<?php echo site_url() ?>assets/images/icon-business-01@2x.png"></div>
              <div class="item-title ch">基礎教育<span></span></div>
              <div class="item-title en">K12 Education</div></a></div>
          <div class="item col-1"><a href="<?php echo site_url('page/41') ?>">
              <div class="item-icon"><img src="<?php echo site_url() ?>assets/images/icon-business-02@2x.png"></div>
              <div class="item-title ch">安博大学<span></span></div>
              <div class="item-title en">Vocational Education</div></a></div>
          <div class="item col-1"><a href="<?php echo site_url('page/42') ?>">
              <div class="item-icon"><img src="<?php echo site_url() ?>assets/images/icon-business-03@2x.png"></div>
              <div class="item-title ch">企业培训<span></span></div>
              <div class="item-title en">Enterprise Training</div></a></div>
          <div class="item col-1"><a href="<?php echo site_url('page/43') ?>">
              <div class="item-icon"><img src="<?php echo site_url() ?>assets/images/icon-business-04@2x.png"></div>
              <div class="item-title ch">終生學習<span></span></div>
              <div class="item-title en">Lifelong Learning</div></a></div>
        </div>
        <div class="index-news">
          <h1 class="sideTitle">AMBOW NEWS</h1>
          <div class="flex">

            <?php if (!empty($top_news)): ?>
            <!-- 列表置顶-->
            <div class="news-list">
              <div class="news-list-item">
                <div class="item-content">
                  <div class="item-cover">
                  <?php if (!empty($top_news['post_cover'])): ?>
                  <img src="<?php echo site_url('upload/' . $top_news['post_cover']) ?>">
                  <?php endif ?></div><a href="<?php echo site_url('news/detail/' . $top_news['id']) ?>">
                    <div class="item-description">><?php echo $top_news['title'] ?></div></a>
                </div>
                <div class="item-bottom">
                  <div class="category"><img src="<?php echo site_url() ?>assets/images/icon-tag.png"/> <?php echo $top_news['tag_title'] ?></div>
                  <div class="date"><img src="<?php echo site_url() ?>assets/images/icon-clock.png"/><?php echo date('j M, Y', strtotime($top_news['createtime'])) ?></div>
                </div>
              </div>
            </div>
            <?php endif ?>

            <div class="news-list">
              <?php foreach ($news['records'] as $value): ?>
              <div class="news-list-item">
                <div class="item-content"><a href="<?php echo site_url('news/detail/' . $value['id']) ?>"> 
                    <div class="item-description"><?php echo $value['title'] ?></div></a></div>
                <div class="item-bottom">
                  <div class="category"><img src="<?php echo site_url() ?>assets/images/icon-tag.png"/><?php echo $value['tag_title'] ?></div>
                  <div class="date"><img src="<?php echo site_url() ?>assets/images/icon-clock@2x.png"/><?php echo date('j M, Y', strtotime($value['createtime'])) ?></div>
                </div>
              </div>
              <?php endforeach ?>
            </div>
          </div>
        </div>
      </div>
    </div>