
    <div id="main-wrapper">
      <h1 class="sideTitle left">AMBOW</h1>
      <h1 class="sideTitle right">EDUCATION</h1>
      <!-- 置顶范本-->
      <div class="news-article-top news-list-item">
        <div class="item-cover">
          <?php if (!empty($news['post_cover'])): ?>
            <img src="<?php echo site_url('upload/' . $news['post_cover']) ?>">
          <?php endif ?>
        </div>
        <div class="item-info">
          <div class="item-content">
            <div class="item-description"><?php echo $news['title'] ?></div>
          </div>
          <div class="item-bottom">
            <div class="category"><img src="<?php echo site_url() ?>assets/images/icon-tag.png"/>动态</div>
            <div class="date"><img src="<?php echo site_url() ?>assets/images/icon-clock@2x.png"/>10 FEB  2019</div>
          </div>
          <?php 
          $urls = array(
            array('key'=> 'post_url_wechat', 'icon'=> 'icon-wechat@2x.png'),
            array('key'=> 'post_url_baijiahao', 'icon'=> 'icon-baidu@2x.png'),
            array('key'=> 'post_url_sina', 'icon'=> 'icon-sina@2x.png'),
            array('key'=> 'post_url_weibo', 'icon'=> 'icon-weibo@2x.png'),
            array('key'=> 'post_url_qq', 'icon'=> 'icon-qq@2x.png'),
            array('key'=> 'post_url_toutiao', 'icon'=> 'icon-toutiao@2x.png'),
            array('key'=> 'post_url_sohu', 'icon'=> 'icon-ss@2x.png'),
          );
          $li = array();
          foreach ($urls as $value) {
            if (!empty($news[$value['key']])) {
              $li[] = '<li><a href="' . $news[$value['key']] . '" target="_blank"><img src="' . site_url('assets/images/' . $value['icon']) . '"></a></li>';
            }
          }
          ?>

          <?php if (!empty($li)): ?>
          <p><a href="#">前往其他平台閱讀</a></p>
          <div class="socialMedia">
            <ol style="justify-content:normal">
              <?php echo join('', $li) ?>
            </ol>
          </div>            
          <?php endif ?>
          
        </div>
      </div>
      
      <?php echo join('', $template_html) ?>

      <div class="news-back"><a href="javascript:history.back();">
          <div class="item-btn">回到列表</div></a></div>
    </div>