'use strict';

var ns = 'urbanArt';

(function () {
  window[ns] = {
    body: document.querySelector('body'),
    indexPage: document.querySelector('.index'),
    spriteImages: document.querySelectorAll('.slide-item__image'),
    spriteImagesSrc: [],
    texts: [],
    islanding: true,
    canvas: {},
    masonryGrid: {},
    prepare: function prepare() {
      this.pushImage();
    },
    init: function init() {
      // console.log('init')
      this.loading();
      this.prepare();
      this.menuToggle();
      this.handlePopupEvent();
      this.detectIndexPageURL();
      if (this.indexPage) {
        this.initCanvas();
        this.handleLandingStart();
      }
      this.scrollEvent();
      if (!isMobile) {
        this.mouseEvent();
      } else {
        this.handleLandingAtMobile();
        this.handlePageTouchEvent();
      }

      this.masonryInit();
      this.lightGallery();
    },

    loading: function loading() {
      imagesLoaded(document.body, function () {
        return document.body.classList.remove('loading');
      });
    },

    detectIndexPageURL: function detectIndexPageURL() {
      var _this = this;
      var landingPage = document.querySelector('.landingPage');
      if (document.querySelector('body.index')) {
        var hash = location.hash;
        console.log('hash:', hash);
        if (hash) {
          var detectInterval = setInterval(function () {
            if (document.querySelector('body.isLanding')) {
              clearInterval(detectInterval);
              setTimeout(function () {
                landingPage.classList.add('hide');
                $('body').removeClass('isLanding').addClass('intro');
                console.log(_this.canvas);
                if (!isMobile) {
                  _this.canvas.moveSlider(1);
                  _this.islanding = false;
                } else {
                  setTimeout(function () {
                    _this.pageScrolling(hash, 1000);
                    _this.islanding = false;
                  }, 1000);
                }
              }, 300);
            }
          }, 100);
        }
      }
    },

    menuToggle: function menuToggle() {
      var body = document.querySelector('body');
      var toggle = document.querySelector('.menuToggle');
      var overlay = document.querySelector('.overlay');
      var offCanvas = document.querySelector('.offcanvas');
      toggle.addEventListener('click', function (e) {
        console.log('toggle menu');
        e.preventDefault();
        body.classList.toggle('overflow');
        this.classList.toggle('is-active');
        offCanvas.classList.toggle('is-active');
      });
      overlay.addEventListener('click', function (e) {
        console.log('overlay');
        toggle.classList.toggle('is-active');
      });
    },

    mouseEvent: function mouseEvent() {
      var _this2 = this;

      document.addEventListener('mousemove', function () {
        _this2.handleMouseMove();
      });
    },

    scrollEvent: function scrollEvent() {
      var _this3 = this;

      document.addEventListener('scroll', function () {
        _this3.handlePageScrollEvent();
      });
    },

    handlePageScrollEvent: function handlePageScrollEvent(event) {
      if (this.indexPage) {
        this.handleIndexScroll();
      }
    },

    handlePageTouchEvent: function handlePageTouchEvent(event) {
      var _this4 = this;

      var ts = void 0,
          te = void 0;
      document.addEventListener('touchstart', function (e) {
        ts = e.touches[0].clientY;
      });
      document.addEventListener('touchend', function (e) {
        te = e.changedTouches[0].clientY;
        _this4.handleLandingTouch(ts, te);
      });
    },

    handleMouseMove: function handleMouseMove() {
      if (this.indexPage) {
        this.handleCircleTrack();
      }
    },

    handleIndexScroll: function handleIndexScroll(e) {
      var windowH = window.innerHeight;
      var supportPageOffset = window.pageXOffset !== undefined;
      var isCSS1Compat = (document.compatMode || "") === "CSS1Compat";
      var y = supportPageOffset ? window.pageYOffset : isCSS1Compat ? document.documentElement.scrollTop : document.body.scrollTop;
      var scrollView = windowH + y - windowH / 2;
      var indexInfo = document.querySelector('.index-info');
      var indexBox = document.querySelector('.index-info');
      var indexHeader = document.querySelector('.index-header');
      var body = document.querySelector('body.index');
      var indexBoxTop = indexBox.getBoundingClientRect().top;
      if (indexBoxTop <= 0) {
        indexHeader.classList.add('is-fixed');
        body.classList.add('headerFixed');
      } else {
        indexHeader.classList.remove('is-fixed');
        body.classList.remove('headerFixed');
      }
      // const goAction = document.querySelector('.goAction');
      // if(scrollView > indexInfo.offsetTop) {
      //     goAction.classList.add('show');
      // } else {
      //     goAction.classList.remove('show');
      // }
    },

    getElementY: function getElementY(query) {
      return window.pageYOffset + document.querySelector(query).getBoundingClientRect().top;
    },

    pageScrolling: function pageScrolling(elem, duration) {
      var headerHeight = document.querySelector('.header').clientHeight;

      var startingY = window.pageYOffset;
      var elementY = this.getElementY(elem);
      var targetY = document.body.scrollHeight - elementY < window.innerHeight ? document.body.scrollHeight - window.innerHeight : elementY;
      var diff = targetY - startingY;

      var easing = function easing(t) {
        return t < .5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
      };
      var start = void 0;

      if (!diff) return;

      window.requestAnimationFrame(function step(timestamp) {
        if (!start) start = timestamp;
        var time = timestamp - start;
        var percent = Math.min(time / duration, 1);
        percent = easing(percent);
        window.scrollTo(0, startingY + diff * percent);
        if (time < duration) {
          window.requestAnimationFrame(step);
        }
      });
    },

    handleLandingAtMobile: function handleLandingAtMobile() {
      var maskImg = document.getElementById('maskImage');
      if (maskImg) {
        maskImg.style.height = '350px';
        maskImg.style.transform = 'scale(1.65) translateY(-160px)';
        maskImg.style.y = '-65px';

        TweenMax.to(circleInside, 1.2, { x: '200px', y: '150px', scale: '1', ease: Power2.easeOut });
      }
    },

    handleLandingTouch: function handleLandingTouch(ts, te) {
      var _this5 = this;

      var landingPage = document.querySelector('.landingPage');
      var supportPageOffset = window.pageXOffset !== undefined;
      var isCSS1Compat = (document.compatMode || "") === "CSS1Compat";
      var y = supportPageOffset ? window.pageYOffset : isCSS1Compat ? document.documentElement.scrollTop : document.body.scrollTop;
      if (this.islanding) {
        if (ts > te + 5) {
          // slide down
          setTimeout(function () {
            landingPage.classList.add('hide');
            _this5.body.classList.remove('isLanding');
            _this5.body.classList.add('index-start');
          }, 300);
        }
      }
    },

    handleCircleTrack: function handleCircleTrack() {
      var circleInside = document.getElementById('circleInside');
      var e = window.event;
      TweenMax.to(circleInside, 1.2, { x: e.pageX, y: e.pageY, ease: Power2.easeOut });
    },

    handleLandingStart: function handleLandingStart() {
      var _this6 = this;

      var landingPage = document.querySelector('.landingPage');
      var circleInside = document.getElementById('circleInside');
      var hash = location.hash;
      console.log('hash:', hash);
      if (hash) {
        console.log('has hash');
      } else {
        landingPage.addEventListener('click', function () {
          if (_this6.islanding) {
            if (isMobile) {
              TweenMax.to(circleInside, 1.8, { scale: 8, transformOrigin: "50% 50%", ease: Power2.easeOut });
              setTimeout(function () {
                landingPage.classList.add('hide');
                _this6.body.classList.remove('isLanding');
                _this6.body.classList.add('index-start');
              }, 100);
            } else {
              TweenMax.to(circleInside, 1, { scale: 12, transformOrigin: "50% 50%", ease: Power2.easeOut });
              landingPage.classList.add('hide');
              _this6.body.classList.remove('isLanding');
              _this6.body.classList.add('index-start');
            }
          }
          _this6.islanding = false;
        });
      }
    },

    pushImage: function pushImage() {
      for (var i = 0; i < this.spriteImages.length; i++) {
        var img = this.spriteImages[i];
        if (img.nextElementSibling) {
          this.texts.push(img.nextElementSibling.innerHTML);
        } else {
          this.texts.push('');
        }
        this.spriteImagesSrc.push(img.getAttribute('src'));
      }
    },

    initCanvas: function initCanvas() {
      var _this = this;
      this.canvas = new this.CanvasSlideshow({
        sprites: _this.spriteImagesSrc,
        displacementImage: './assets/images/dmaps_clouds.jpg',
        autoPlay: true,
        autoPlaySpeed: [4, 3],
        displaceScale: [1000, 1500],
        interactive: true,
        interactionEvent: 'click', // 'click', 'hover', 'both' 
        displaceAutoFit: false,
        dispatchPointerOver: true // restarts pointerover event after click 
      }, _this);
    },

    CanvasSlideshow: function CanvasSlideshow(options, obj) {
      var _this = obj;
      var that = this;
      // console.log(_this)
      options = options || {};
      options.stageWidth = options.hasOwnProperty('stageWidth') ? options.stageWidth : 1920;
      options.stageHeight = options.hasOwnProperty('stageHeight') ? options.stageHeight : 1080;
      options.pixiSprites = options.hasOwnProperty('sprites') ? options.sprites : [];
      options.centerSprites = options.hasOwnProperty('centerSprites') ? options.centerSprites : false;
      options.texts = options.hasOwnProperty('texts') ? options.texts : [];
      options.autoPlay = options.hasOwnProperty('autoPlay') ? options.autoPlay : true;
      options.autoPlaySpeed = options.hasOwnProperty('autoPlaySpeed') ? options.autoPlaySpeed : [10, 3];
      options.fullScreen = options.hasOwnProperty('fullScreen') ? options.fullScreen : true;
      options.displaceScale = options.hasOwnProperty('displaceScale') ? options.displaceScale : [200, 70];
      options.displacementImage = options.hasOwnProperty('displacementImage') ? options.displacementImage : '';
      options.navElement = options.hasOwnProperty('navElement') ? options.navElement : document.querySelectorAll('.scene-nav');
      options.displaceAutoFit = options.hasOwnProperty('displaceAutoFit') ? options.displaceAutoFit : false;
      options.wacky = options.hasOwnProperty('wacky') ? options.wacky : false;
      options.interactive = options.hasOwnProperty('interactive') ? options.interactive : false;
      options.interactionEvent = options.hasOwnProperty('interactionEvent') ? options.interactionEvent : '';
      options.displaceScaleTo = options.autoPlay === false ? [0, 0] : [20, 20];
      options.textColor = options.hasOwnProperty('textColor') ? options.textColor : '#fff';
      options.displacementCenter = options.hasOwnProperty('displacementCenter') ? options.displacementCenter : false;
      options.dispatchPointerOver = options.hasOwnProperty('dispatchPointerOver') ? options.dispatchPointerOver : false;

      var renderer = new PIXI.autoDetectRenderer(options.stageWidth, options.stageHeight, { transparent: true });
      var stage = new PIXI.Container();
      var slidesContainer = new PIXI.Container();
      var displacementSprite = new PIXI.Sprite.fromImage(options.displacementImage);
      var displacementFilter = new PIXI.filters.DisplacementFilter(displacementSprite);

      var style = new PIXI.TextStyle({
        fill: options.textColor,
        wordWrap: true,
        wordWrapWidth: 400,
        letterSpacing: 20,
        fontSize: 14
      });

      this.currentIndex = 0;

      this.initPixi = function () {
        var landingCanvas = document.getElementById('mainCanvas');
        // Add canvas to the HTML
        // document.body.appendChild( renderer.view );
        landingCanvas.appendChild(renderer.view);

        // Add child container to the main container 
        stage.addChild(slidesContainer);

        // Enable Interactions
        stage.interactive = true;

        // Fit renderer to the screen
        if (options.fullScreen === true) {
          renderer.view.style.objectFit = 'cover';
          renderer.view.style.width = '100%';
          renderer.view.style.height = '100%';
          renderer.view.style.top = '50%';
          renderer.view.style.left = '50%';
          renderer.view.style.webkitTransform = 'translate( -50%, -50% ) scale(1)';
          renderer.view.style.transform = 'translate( -50%, -50% ) scale(1)';
        } else {
          renderer.view.style.maxWidth = '100%';
          renderer.view.style.top = '50%';
          renderer.view.style.left = '50%';
          renderer.view.style.webkitTransform = 'translate( -50%, -50% )';
          renderer.view.style.transform = 'translate( -50%, -50% )';
        }

        displacementSprite.texture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT;

        // Set the filter to stage and set some default values for the animation
        stage.filters = [displacementFilter];

        if (options.autoPlay === false) {
          displacementFilter.scale.x = 0;
          displacementFilter.scale.y = 0;
        }

        if (options.wacky === true) {

          displacementSprite.anchor.set(0.5);
          displacementSprite.x = renderer.width / 2;
          displacementSprite.y = renderer.height / 2;
        }

        displacementSprite.scale.x = 2;
        displacementSprite.scale.y = 2;

        // PIXI tries to fit the filter bounding box to the renderer so we optionally bypass
        displacementFilter.autoFit = options.displaceAutoFit;

        stage.addChild(displacementSprite);
      };

      this.loadPixiSprites = function (sprites) {

        var rSprites = options.sprites;
        var rTexts = options.texts;

        for (var i = 0; i < rSprites.length; i++) {

          var texture = new PIXI.Texture.fromImage(sprites[i]);
          var image = new PIXI.Sprite(texture);

          if (rTexts) {
            var richText = new PIXI.Text(rTexts[i], style);
            image.addChild(richText);

            richText.anchor.set(0.5);
            richText.x = image.width / 2;
            richText.y = image.height / 2;
          }

          if (options.centerSprites === true) {
            image.anchor.set(0.5);
            image.x = renderer.width / 2;
            image.y = renderer.height / 2;
          }
          // image.transform.scale.x = 1.3;
          // image.transform.scale.y = 1.3;


          if (i !== 0) {
            TweenMax.set(image, { alpha: 0 });
          }

          slidesContainer.addChild(image);
        }
      };

      if (options.autoPlay === true) {

        var ticker = new PIXI.ticker.Ticker();

        ticker.autoStart = options.autoPlay;

        ticker.add(function (delta) {

          displacementSprite.x += options.autoPlaySpeed[0] * delta;
          displacementSprite.y += options.autoPlaySpeed[1];

          renderer.render(stage);
        });
      } else {

        var render = new PIXI.ticker.Ticker();

        render.autoStart = true;

        render.add(function (delta) {
          renderer.render(stage);
        });
      };

      var isPlaying = false;
      var slideImages = slidesContainer.children;
      this.moveSlider = function (newIndex) {

        isPlaying = true;

        var baseTimeline = new TimelineMax({ onComplete: function onComplete() {
            that.currentIndex = newIndex;
            isPlaying = false;
            if (options.wacky === true) {
              displacementSprite.scale.set(3);
            }
          }, onUpdate: function onUpdate() {

            if (options.wacky === true) {
              displacementSprite.rotation += baseTimeline.progress() * 0.02;
              displacementSprite.scale.set(baseTimeline.progress() * 3);
            }
          } });

        baseTimeline.clear();

        if (baseTimeline.isActive()) {
          return;
        }

        // DEMO 4
        baseTimeline.to(displacementFilter.scale, 0.8, { x: options.displaceScale[0], y: options.displaceScale[1], ease: Power2.easeIn }).to(slideImages[that.currentIndex], 0.5, { alpha: 0, ease: Power2.easeOut }, 0.4).to(slideImages[newIndex], 0.8, { alpha: 1, ease: Power2.easeOut }, 1).to(displacementFilter.scale, 0.7, { x: options.displaceScaleTo[0], y: options.displaceScaleTo[1], ease: Power1.easeOut }, 0.9);
      };

      // mouse wheel event
      var wheelEvent;
      if ('onwheel' in window) {
        wheelEvent = 'wheel';
      } else if (document.onmousewheel !== undefined) {
        wheelEvent = 'mousewheel';
      } else {
        wheelEvent = 'DOMMouseScroll';
      }

      window.addEventListener(wheelEvent, mouseWheel, false);
      if (!isMobile) {
        if (window.addEventListener) {
          // IE9+, Chrome, Safari, Opera
          window.addEventListener("mousewheel", mouseWheel, false);
          // Firefox
          window.addEventListener("DOMMouseScroll", mouseWheel, false);
          bindMask();
        } else {
          // // IE 6/7/8
          window.attachEvent("onmousewheel", mouseWheel);
        }
      }

      if (!isMobile) {
        bindMask();
      }

      function bindMask() {
        var viewMore = document.getElementById('js-viewMore');
        viewMore.addEventListener('click', function () {
          that.moveSlider(that.currentIndex + 1);
          _this.body.classList.add('intro');
          _this.body.classList.remove('index-start');
        });
      }

      function mouseWheel(event) {
        // let delta = 0;
        var e = window.event || event;
        var scrollVal = $(window).scrollTop();
        // console.log(scrollVal)
        var delta = Math.max(-1, Math.min(1, e.wheelDelta || -e.detail));

        console.log(delta);

        if (isPlaying) {
          return false;
        }

        if (!_this.islanding) {
          if (delta <= -1) {
            // mouse scroll down

            if (that.currentIndex >= 0 && that.currentIndex < slideImages.length - 1) {
              that.moveSlider(that.currentIndex + 1);
              _this.body.classList.add('intro');
              _this.body.classList.remove('index-start');
            }
          } else {
            // mouse scroll up

            if (that.currentIndex > 0 && that.currentIndex < slideImages.length && scrollVal === 0) {
              that.moveSlider(that.currentIndex - 1);
              _this.body.classList.remove('intro');
              _this.body.classList.add('index-start');
            }
            console.log('currentIndex:', that.currentIndex);
          }
        } else {
          return false;
        }
      }

      // click nav 
      // var nav = options.navElement;
      // for ( var i = 0; i < nav.length; i++ ) {

      //   var navItem = nav[i];

      //   navItem.onclick = function( event ) {

      //     // Make sure the previous transition has ended
      //     if ( isPlaying ) {
      //       return false;
      //     }     

      //     if ( this.getAttribute('data-nav') === 'next' ) {

      //       if ( that.currentIndex >= 0 && that.currentIndex < slideImages.length - 1 ) {
      //         that.moveSlider( that.currentIndex + 1 );
      //       } else {
      //         that.moveSlider( 0 );
      //       }

      //     } else {

      //       if ( that.currentIndex > 0 && that.currentIndex < slideImages.length ) {
      //         that.moveSlider( that.currentIndex - 1 );
      //       } else {
      //         that.moveSlider( spriteImages.length - 1 );
      //       }            

      //     }

      //     return false;

      //   }
      // };

      this.init = function () {
        console.log('init canvas');
        that.initPixi();
        that.loadPixiSprites(options.pixiSprites);
      };

      function rotateSpite() {
        displacementSprite.rotation += 0.001;
        rafID = requestAnimationFrame(rotateSpite);
      }

      if (options.interactive === true) {

        var rafID, mouseX, mouseY;

        // Enable interactions on our slider
        slidesContainer.interactive = true;
        slidesContainer.buttonMode = true;

        // HOVER
        if (options.interactionEvent === 'hover' || options.interactionEvent === 'both') {

          slidesContainer.pointerover = function (mouseData) {
            mouseX = mouseData.data.global.x;
            mouseY = mouseData.data.global.y;
            TweenMax.to(displacementFilter.scale, 1, { x: "+=" + Math.sin(mouseX) * 100 + "", y: "+=" + Math.cos(mouseY) * 100 + "" });
            rotateSpite();
          };

          slidesContainer.pointerout = function (mouseData) {
            TweenMax.to(displacementFilter.scale, 1, { x: 0, y: 0 });
            cancelAnimationFrame(rafID);
          };
        }

        // CLICK
        if (options.interactionEvent === 'click' || options.interactionEvent === 'both') {

          slidesContainer.pointerup = function (mouseData) {
            if (options.dispatchPointerOver === true) {
              TweenMax.to(displacementFilter.scale, 1, { x: 0, y: 0, onComplete: function onComplete() {
                  TweenMax.to(displacementFilter.scale, 1, { x: 20, y: 20 });
                } });
            } else {
              TweenMax.to(displacementFilter.scale, 1, { x: 0, y: 0 });
              cancelAnimationFrame(rafID);
            }
          };

          slidesContainer.pointerdown = function (mouseData) {
            mouseX = mouseData.data.global.x;
            mouseY = mouseData.data.global.y;
            TweenMax.to(displacementFilter.scale, 1, { x: "+=" + Math.sin(mouseX) * 1200 + "", y: "+=" + Math.cos(mouseY) * 200 + "" });
          };
        }
      };

      if (options.displacementCenter === true) {
        displacementSprite.anchor.set(0.5);
        displacementSprite.x = renderer.view.width / 2;
        displacementSprite.y = renderer.view.height / 2;
      }

      this.init();

      function scaleToWindow(canvas, backgroundColor) {
        var scaleX, scaleY, scale, center;

        //1. Scale the canvas to the correct size
        //Figure out the scale amount on each axis
        scaleX = window.innerWidth / canvas.offsetWidth;
        scaleY = window.innerHeight / canvas.offsetHeight;

        //Scale the canvas based on whichever value is less: `scaleX` or `scaleY`
        scale = Math.min(scaleX, scaleY);
        canvas.style.transformOrigin = "0 0";
        canvas.style.transform = "scale(" + scale + ")";

        //2. Center the canvas.
        //Decide whether to center the canvas vertically or horizontally.
        //Wide canvases should be centered vertically, and 
        //square or tall canvases should be centered horizontally
        if (canvas.offsetWidth > canvas.offsetHeight) {
          if (canvas.offsetWidth * scale < window.innerWidth) {
            center = "horizontally";
          } else {
            center = "vertically";
          }
        } else {
          if (canvas.offsetHeight * scale < window.innerHeight) {
            center = "vertically";
          } else {
            center = "horizontally";
          }
        }

        //Center horizontally (for square or tall canvases)
        var margin;
        if (center === "horizontally") {
          margin = (window.innerWidth - canvas.offsetWidth * scale) / 2;
          canvas.style.marginTop = 0 + "px";
          canvas.style.marginBottom = 0 + "px";
          canvas.style.marginLeft = margin + "px";
          canvas.style.marginRight = margin + "px";
        }

        //Center vertically (for wide canvases) 
        if (center === "vertically") {
          margin = (window.innerHeight - canvas.offsetHeight * scale) / 2;
          canvas.style.marginTop = margin + "px";
          canvas.style.marginBottom = margin + "px";
          canvas.style.marginLeft = 0 + "px";
          canvas.style.marginRight = 0 + "px";
        }

        //3. Remove any padding from the canvas  and body and set the canvas
        //display style to "block"
        canvas.style.paddingLeft = 0 + "px";
        canvas.style.paddingRight = 0 + "px";
        canvas.style.paddingTop = 0 + "px";
        canvas.style.paddingBottom = 0 + "px";
        canvas.style.display = "block";

        //4. Set the color of the HTML body background
        document.body.style.backgroundColor = backgroundColor;

        //Fix some quirkiness in scaling for Safari
        var ua = navigator.userAgent.toLowerCase();
        if (ua.indexOf("safari") != -1) {
          if (ua.indexOf("chrome") > -1) {
            // Chrome
          } else {
              // Safari
              //canvas.style.maxHeight = "100%";
              //canvas.style.minHeight = "100%";
            }
        }

        //5. Return the `scale` value. This is important, because you'll nee this value 
        //for correct hit testing between the pointer and sprites
        return scale;
      }
    },

    handlePopupEvent: function handlePopupEvent() {
      var _this7 = this;

      var subscribe = document.querySelector('.subscribeIcon');
      var closePopup = document.querySelector('.popup-overlay');
      if (subscribe) {
        subscribe.addEventListener('click', function () {
          _this7.openPopup('subscribe');
        });
      }
      if (closePopup) {
        closePopup.addEventListener('click', function () {
          _this7.closePopup();
        });
      }
    },

    closePopup: function closePopup() {
      console.log('close popup');
      var popup = document.querySelector('.popup');
      popup.classList.remove('is-open');
    },

    openPopup: function openPopup(id) {
      var popup = document.getElementById(id);
      popup.classList.add('is-open');
    },

    masonryInit: function masonryInit() {
      var artistList = document.querySelector('.artist-list');
      var worksList = document.querySelector('.works-list');
      if (artistList || worksList) {
        var $grid = $('.grid').imagesLoaded(function () {
          $grid.masonry({
            itemSelector: '.grid-item',
            percentPosition: true,
            columnWidth: '.grid-sizer',
            gutter: '.gutter-sizer',
            horizontalOrder: true
          });
        });
        this.masonryGrid = $grid;
      }
      this.handleChangeView();
      console.log(this.masonryGrid);
    },

    handleChangeView: function handleChangeView() {
      var _this = this;
      var artistList = document.querySelector('.artist-list');
      if (artistList) {
        $('.js-viewControl').on('click', function () {
          var dataView = $(this).data('view');
          console.log(artistList.classList[1]);
          var currentClass = artistList.classList[1];
          artistList.classList.remove(currentClass);
          artistList.classList.add(dataView);
          _this.masonryInit();
          $('.js-viewControl').removeClass('is-active');
          $(this).addClass('is-active');
        });
      }
    },

    lightGallery: function (_lightGallery) {
      function lightGallery() {
        return _lightGallery.apply(this, arguments);
      }

      lightGallery.toString = function () {
        return _lightGallery.toString();
      };

      return lightGallery;
    }(function () {
      var workGallery = document.getElementById('workGallery');
      if (workGallery) {
        console.log('init light gallery');
        lightGallery(workGallery, {
          selector: '.cover',
          download: false,
          fullScreen: false,
          subHtmlSelectorRelative: true,
          counter: false
        });
      }
    })
  };
})();

$(function () {
  window.urbanArt.init();
});