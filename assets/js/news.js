"use strict";

var target = document.querySelectorAll(".listSection .unit");
var wave_stage = void 0,
    wave_renderer = void 0,
    wave_timer = void 0,
    wave_DisplacementSprite = void 0;

function createWaveEffect(el) {
  var paintingImage = el.querySelector(".picSection img");
  var nowWidth = paintingImage.width;
  var nowHeight = paintingImage.height;
  var orignWidth = paintingImage.naturalWidth;
  var orignHeight = paintingImage.naturalHeight;
  var textureScaleX = nowWidth / orignWidth;
  var textureScaleY = nowHeight / orignHeight;
  var paintingImageSrc = paintingImage.getAttribute("src");

  wave_renderer = PIXI.autoDetectRenderer(nowWidth, nowHeight);
  wave_renderer.view.className = "rendererView";
  el.querySelector(".picSection").appendChild(wave_renderer.view);

  wave_stage = new PIXI.Container();

  var paintingImageTexture = PIXI.Texture.fromImage(paintingImageSrc);
  var paintingImageSprite = new PIXI.Sprite(paintingImageTexture);
  paintingImageSprite.scale.x = textureScaleX;
  paintingImageSprite.scale.y = textureScaleY;

  wave_stage.addChild(paintingImageSprite);

  wave_DisplacementSprite = PIXI.Sprite.fromImage("assets/images/displacementMap.jpg");

  var repeatTexture = PIXI.Texture.fromImage("assets/images/displacementMap.jpg");
  repeatTexture.baseTexture.wrapMode = PIXI.WRAP_MODES.REPEAT;

  var displacementFilter = new PIXI.filters.DisplacementFilter(wave_DisplacementSprite);
  // 變形幅度
  displacementFilter.scale.x = 20;
  displacementFilter.scale.y = 20;

  wave_stage.addChild(wave_DisplacementSprite);

  wave_stage.filters = [displacementFilter];
  wave_timer = requestAnimationFrame(animate);
}

function animate() {
  wave_DisplacementSprite.x += 0.1 * 5;
  wave_DisplacementSprite.y += 0.1 * 5;

  wave_renderer.render(wave_stage);

  wave_timer = requestAnimationFrame(animate);
}

function removeWaveEffect() {
  wave_stage.destroy(true);
  wave_stage = null;
  wave_renderer.destroy(true);
  wave_renderer = null;
  cancelAnimationFrame(wave_timer);
}

function bindEvnet() {
  var _loop = function _loop(i) {
    target[i].removeEventListener("mouseenter", function () {
      createWaveEffect(target[i]);
    }, false);
    target[i].addEventListener("mouseenter", function () {
      createWaveEffect(target[i]);
    }, false);
    target[i].removeEventListener("mouseleave", function () {
      removeWaveEffect();
    }, false);
    target[i].addEventListener("mouseleave", function () {
      removeWaveEffect();
    }, false);
  };

  for (var i = 0; i < target.length; i++) {
    _loop(i);
  }
}
bindEvnet();

function doSomething(e) {
  var rightclick;
  if (!e) var e = window.event;
  if (e.which) rightclick = e.which == 3;else if (e.button) rightclick = e.button == 2;
  alert("Rightclick: " + rightclick); // true or false
}

function viewport() {
  var e = window,
      a = "inner";
  if (!("innerWidth" in window)) {
    a = "client";
    e = document.documentElement || document.body;
  }
  return { width: e[a + "Width"], height: e[a + "Height"] };
}

window.addEventListener("scroll", function () {
  var wH = window.innerHeight;
  var st = document.body.scrollTop && document.body.scrollTop !== 0 ? document.body.scrollTop : document.documentElement.scrollTop;
  var target = document.querySelectorAll(".contentPic");

  for (var i = 0; i < target.length; i++) {
    var rect = target[i].getBoundingClientRect();
    if (rect.top - wH / 2 < 0) {
      target[i].classList.add("scale");
    } else {
      target[i].classList.remove("scale");
    }
  }

  var titleBox = document.querySelector(".titleContainer");
  if (titleBox) {
    var title = titleBox.querySelector(".titleBox");
    var titlePosition = titleBox.getBoundingClientRect();
    var fixeNavH = document.querySelector(".fixedTitle").offsetHeight;

    if (viewport().width > 1024) {
      console.log(titlePosition.top);
      if (titlePosition.top <= fixeNavH) {
        title.style.position = "fixed";
        title.style.top = fixeNavH + "px";
      } else {
        title.style.position = "static";
        title.style.top = "auto";
      }
    } else {
      title.style.position = "static";
      title.style.top = "auto";
    }
  }
});

document.body.oncontextmenu = function () {
  return false;
};