
CREATE
    /*[ALGORITHM = {UNDEFINED | MERGE | TEMPTABLE}]
    [DEFINER = { user | CURRENT_USER }]
    [SQL SECURITY { DEFINER | INVOKER }]*/
    VIEW `F06_view` 
    AS
(
SELECT
  `x1`.`id`          AS `id`,
  `x1`.`post_type`   AS `post_type`,
  `x1`.`status`      AS `status`,
  DATE_FORMAT(`x1`.`createtime`,'%Y-%m-%d') AS `createtime`,
  `x1`.`title`       AS `title`,
  `a1`.`tag_id`      AS `tag_id`,
  `a1`.`sort`        AS `sort`,
  `a2`.`title`       AS `tag_title`,
  `a2`.`tag_type`    AS `tag_type`,
  `x2`.`meta_value`  AS `post_title`,
  `x3`.`meta_value`  AS `post_cover`,
  `x4`.`meta_value`  AS `post_content`,
  `x6`.`meta_value`  AS `post_link`,
  `x7`.`meta_value`  AS `post_link_target`
FROM `posts` `x1`
JOIN `tag_assoc` `a1` ON `a1`.`post_id` = `x1`.`id`
LEFT JOIN `tags` `a2` ON `a2`.`id` = `a1`.`tag_id`
LEFT JOIN `tags` `a3` ON `a3`.`id` = `a2`.`parent_id`
LEFT JOIN `meta` `x2` ON `x2`.`parent_id` = `x1`.`id` AND `x2`.`meta_key` = 'post_title' 
LEFT JOIN `meta` `x3` ON `x3`.`parent_id` = `x1`.`id` AND `x3`.`meta_key` = 'post_cover'
LEFT JOIN `meta` `x4` ON `x4`.`parent_id` = `x1`.`id` AND `x4`.`meta_key` = 'post_content'
LEFT JOIN `meta` `x6` ON `x6`.`parent_id` = `x1`.`id` AND `x6`.`meta_key` = 'post_link'
LEFT JOIN `meta` `x7` ON `x7`.`parent_id` = `x1`.`id` AND `x7`.`meta_key` = 'post_link_target'
WHERE `x1`.`post_type` = 'F06'
);
